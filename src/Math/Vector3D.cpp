/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#include "_ml_base_def.h"
#include "Vector3D.h"

#include "_ml_assert.h"

#include <sstream>

/************ Vec3D ************/
template<typename T>
Vec3D<T>::Vec3D(void)
: x(0.0), y(0.0), z(0.0), c(0.0)
//: x(data[0]), y(data.y), z(data.z), c(data[3])
{
  /*x = 0.0;
  y = 0.0;
  z = 0.0;
  c = 0.0;*/
}

template<typename T>
Vec3D<T>::Vec3D(T _x, T _y, T _z, T _c)
: x(_x), y(_y), z(_z), c(_c)
//: x(data[0]), y(data.y), z(data.z), c(data[3])
{
  /*x = _x;
  y = _y;
  z = _z;
  c = _c;*/
}

template<typename T>
void
Vec3D<T>::Set(T _x, T _y, T _z)
{
  x = _x; y = _y; z = _z;
}

template<typename T>
T
Vec3D<T>::Length(void) const
{
  return SQRT(SQR(x) + SQR(y) + SQR(z));
}

template<typename T>
void
Vec3D<T>::Normalize(void)
{
  T l = Length();
  x /= l;
  y /= l;
  z /= l;
}

template<typename T>
T&
Vec3D<T>::operator[](int i)
{
  switch(i)
  {
  case 0: return x;
  case 1: return y;
  case 2: return z;
  case 3: return c; // this is a bit dangerous! This allows modification of c!
  default:
    _ML_ASSERT(i <= 3);
    _ML_ASSERT(i >= 0);
    return c; // should never happen; no runtime check - assertions should be sufficient
  }
}

template <typename T>
const T&
Vec3D<T>::operator[](int i) const
{
  switch(i)
  {
  case 0: return x;
  case 1: return y;
  case 2: return z;
  case 3: return c; // this is a bit dangerous! This allows modification of c!
  default:
    _ML_ASSERT(i <= 3);
    _ML_ASSERT(i >= 0);
    return c; // should never happen; no runtime check - assertions should be sufficient
  }
}

template<typename T>
Vec3D<T>&
Vec3D<T>::operator*=(T s)
{
  x *= s;
  y *= s;
  z *= s;
  return *this;
}

template<typename T>
Vec3D<T>&
Vec3D<T>::operator/=(T s)
{
  x /= s;
  y /= s;
  z /= s;
  return *this;
}

template<typename T>
Vec3D<T>&
Vec3D<T>::operator-(void)
{
  x = -x;
  y = -y;
  z = -z;
  return *this;
}

//template<typename T>
//void
//Vec3D<T>::operator=(const Vec3D<T>& source)
//{
//  x = source.x;
//  y = source.y;
//  z = source.z;
//  c = source.c;
//}

template<typename T>
std::string
Vec3D<T>::ToString(void) const
{
  std::ostringstream s;
  s << "(" << x << "," << y << "," << z << ")";
  return s.str();
}

template<typename T>
void
Vec3D<T>::RotateX(T angle) //Pitch
{
  //         (1    0      0    0)
  // Rx(q) = (0  cos q  sin q  0)
  //         (0 -sin q  cos q  0)
  //         (0    0     0     1)
  const T _y = y; const T _z = z;
  x = x;
  y = (_y * cos(angle)) - (_z * sin(angle));
  z = (_y * sin(angle)) + (_z * cos(angle));
}

template<typename T>
void
Vec3D<T>::RotateY(T angle) //Yaw
{
  //         (cos q  0  -sin q   0)
  // Ry(q) = (0      1    0      0)
  //         (sin q  0  cos q    0)
  //         (0      0    0      1)
  const T _x = x; const T _z = z;
  x = (_z * sin(angle)) + (_x * cos(angle));
  y = y;
  z = (_z * cos(angle)) - (_x * sin(angle));
}

template<typename T>
void
Vec3D<T>::RotateZ(T angle) //Roll
{
  //          ( cos q  sin q  0  0)
  // Rz (q) = (-sin q  cos q  0  0)
  //          ( 0        0    1  0)
  //          ( 0        0    0  1)
  const T _x = x; const T _y = y;
  x = (_x * cos(angle)) - (_y * sin(angle));
  y = (_x * sin(angle)) + (_y * cos(angle));
  z = z;
}

template <typename T>
std::ostream& operator<<(std::ostream &os, const Vec3D<T>& v)
{
  os << "(" << v.x << "," << v.y << "," << v.z << "," << v.c << ")";
  return os;
}

template <typename T>
std::istream& operator>>(std::istream &is, Vec3D<T>& v)
{
  char c;

#define __STREAM_READIN(prefix,var) is >> c;\
    if (c == prefix)\
      is >> var;\
    else\
    {\
      is.clear(std::ios::badbit);\
      return is;\
    }

  __STREAM_READIN('(',v.x);
  __STREAM_READIN(',',v.y);
  __STREAM_READIN(',',v.z);
  __STREAM_READIN(',',v.c);

  is >> c;
  if (c != ')')
    is.clear(std::ios::badbit);

#undef __STREAM_READIN

  return is;
}

/************ Vector3D ************/

template<typename T>
Vector3D<T>::Vector3D(void)
: Vec3D<T>(0.0, 0.0, 0.0, 1.0)
{
}
template<typename T>
Vector3D<T>::Vector3D(T _x, T _y, T _z)
: Vec3D<T>(_x, _y, _z, 1.0)
{
}
template<typename T>
Vector3D<T>::Vector3D(const Vector3D& v)
: Vec3D<T>(v.x, v.y, v.z, 1.0)
{
}

/************ Point3D ************/

template<typename T>
Point3D<T>::Point3D(void)
: Vec3D<T>(0.0, 0.0, 0.0, 0.0)
{
}
template<typename T>
Point3D<T>::Point3D(T _x, T _y, T _z)
: Vec3D<T>(_x, _y, _z, 0.0)
{
}
template<typename T>
Point3D<T>::Point3D(const Point3D& p)
: Vec3D<T>(p.x, p.y, p.z, 0.0)
{
}

/************ OPERATIONS ***********/
template <typename T>
Vector3D<T> CrossProduct(const Vector3D<T>& u, const Vector3D<T>& v)
{
  return Vector3D<T>(u.y*v.z - u.z*v.y, u.z*v.x - u.x*v.z, u.x*v.y - u.y*v.x);
}

template <typename T>
T DotProduct(const Vector3D<T>& u, const Vector3D<T>& v)
{
  return u.DotProduct(v);
}

/************ OPERATORS ************/
template <typename T>
Vector3D<T> operator*(const Vector3D<T>& v, T s)
{
  Vector3D<T> _v(v);
  _v *= s;
  return _v;
}

template <typename T>
Vector3D<T> operator*(T s, const Vector3D<T>& v)
{
  Vector3D<T> _v(v);
  _v *= s;
  return _v;
}

template <typename T>
Vector3D<T> operator*(const Vector3D<T>& v, unsigned int i)
{
  return (v * (T)i);
}

template <typename T>
Vector3D<T> operator*(unsigned int i, const Vector3D<T>& v)
{
  return (v * (T)i);
}

template <typename T>
Point3D<T> operator+(const Point3D<T>& p, const Vector3D<T>& v)
{
  /*Point3D<T> _p(p);
  _p.x += v.x;
  _p.y += v.y;
  _p.z += v.z;
  return _p;*/
  return Point3D<T>(p.x + v.x, p.y + v.y, p.z + v.z); // faster
}

template <typename T>
Point3D<T> operator-(const Point3D<T>& p, const Vector3D<T>& v)
{
  /*Point3D<T> _p(p);
  _p.x -= v.x;
  _p.y -= v.y;
  _p.z -= v.z;
  return _p;*/
  return Point3D<T>(p.x - v.x, p.y - v.y, p.z - v.z); // faster
}

template <typename T>
Vector3D<T> operator+(const Vector3D<T>& v1, const Vector3D<T>& v2)
{
  /*Vector3D<T> _v(v1);
  _v.x += v2.x;
  _v.y += v2.y;
  _v.z += v2.z;
  return _v;*/
  return Vector3D<T>(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z); // faster
}

template <typename T>
Vector3D<T> operator-(const Vector3D<T>& v1, const Vector3D<T>& v2)
{
  /*Vector3D<T> _v(v1);
  _v.x -= v2.x;
  _v.y -= v2.y;
  _v.z -= v2.z;
  return _v;*/
  return Vector3D<T>(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z); // faster
}

template <typename T>
Vector3D<T> operator+(const Point3D<T>& p1, const Point3D<T>& p2)
{
  /*Vector3D<T> _v;
  _v.x = p1.x + p2.x;
  _v.y = p1.y + p2.y;
  _v.z = p1.z + p2.z;
  return _v;*/
  return Vector3D<T>(p1.x + p2.x, p1.y + p2.y, p1.z + p2.z); // faster
}

template <typename T>
Vector3D<T> operator-(const Point3D<T>& p1, const Point3D<T>& p2)
{
  /*Vector3D<T> _v;
  _v.x = p1.x - p2.x;
  _v.y = p1.y - p2.y;
  _v.z = p1.z - p2.z;
  return _v;*/
  return Vector3D<T>(p1.x - p2.x, p1.y - p2.y, p1.z - p2.z); // faster
}

/* explicit template typename instantiations */
//template struct Vec3D<int>;
template struct Vec3D<float>;
template struct Vec3D<double>;
//template struct Point3D<int>;
template struct Point3D<float>;
template struct Point3D<double>;
//template struct Vector3D<int>;
template struct Vector3D<float>;
template struct Vector3D<double>;

__INSTANTIATE_OPERATORS(float);
__INSTANTIATE_OPERATORS(double);

//template void Point3D<double>::Set(const Point3D<float>& p);
//template void Point3D<float>::Set(const Point3D<double>& p);
//template void Point3D<float>::Set(const Point3D<float>& p);
//template void Vector3D<double>::Set(const Vector3D<float>& v);
//template void Vector3D<float>::Set(const Vector3D<double>& v);
//template void Vector3D<float>::Set(const Vector3D<float>& v);

template void Vec3D<double>::Set(const Vec3D<float>& v);
template void Vec3D<float>::Set(const Vec3D<float>& v);
template void Vec3D<double>::Set(const Vec3D<double>& v);
template void Vec3D<float>::Set(const Vec3D<double>& v);
