/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

/** @file ArrayStatistics.h
 *
 * Provide basic statistical measures for arrays.
 * - mean (arithmetic and quadratic)
 * - variance/standard deviation (sample and true)
 *
 * @author Boris Schauerte
 * @date 2009
 */
#pragma once

#include "_ml_assert.h"

/** Calculate the arithmetic mean. */
template <typename BASE_T, typename INTERMEDIATE_T>
BASE_T
MeanArithmetic(const BASE_T* data, unsigned int size)
{
  INTERMEDIATE_T tmp = 0; // intermediate precision should be higher than base precision
  for (unsigned int i = 0; i < size; i++)
  {
    tmp += (INTERMEDIATE_T)data[i];
  }
  return (BASE_T)(tmp / (INTERMEDIATE_T)size);
}

/** Calculate the quadratic mean. */
template <typename BASE_T, typename INTERMEDIATE_T>
BASE_T
MeanQuadratic(const BASE_T* data, unsigned int size)
{
  INTERMEDIATE_T tmp = 0; // intermediate precision should be higher than base precision
  for (unsigned int i = 0; i < size; i++)
  {
    tmp += (INTERMEDIATE_T)SQR(data[i]);
  }
  return (BASE_T)SQRT(tmp / (INTERMEDIATE_T)size);
}

/** Calculate the quadratic mean using its relation with arithmetic mean and standard deviation.
 * Appropriate (only) if standardDeviation is for the whole population and NOT a sample.
 */
template <typename BASE_T>
BASE_T
MeanQuadratic(BASE_T arithmeticMean, BASE_T standardDeviation)
{
  return SQRT(SQR(arithmeticMean) + SQR(standardDeviation));
}

/** Calculate the "sample variance" (uses 1/N-1).
 * Appropriate if the data contains samples of a population.
 */
template <typename BASE_T, typename INTERMEDIATE_T>
BASE_T
VarianceSample(const BASE_T* data, const BASE_T mean, unsigned int size)
{
  _ML_ASSERT(size > 1);

  INTERMEDIATE_T tmp = 0; // intermediate precision should be higher than base precision
  for (unsigned int i = 0; i < size; i++)
  {
    tmp += (INTERMEDIATE_T)SQR(data[i] - mean);
  }
  return (BASE_T)(tmp / (INTERMEDIATE_T)(size - 1));
}

/** Calculate the "true variance" (uses 1/N).
 * Appropriate if the data contains the whole finite population.
 */
template <typename BASE_T, typename INTERMEDIATE_T>
BASE_T
VarianceTrue(const BASE_T* data, const BASE_T mean, unsigned int size)
{
  _ML_ASSERT(size > 0);

  INTERMEDIATE_T tmp = 0; // intermediate precision should be higher than base precision
  for (unsigned int i = 0; i < size; i++)
  {
    tmp += (INTERMEDIATE_T)SQR(data[i] - mean);
  }
  return (BASE_T)(tmp / (INTERMEDIATE_T)(size));
}

/** Calculate the (sample) standard deviation. */
template <typename BASE_T, typename INTERMEDIATE_T>
BASE_T
StandardDeviationSample(const BASE_T* data, const BASE_T mean, unsigned int size)
{
  return SQRT(VarianceSample(data, mean, size));
}

/** Calculate the (true) standard deviation. */
template <typename BASE_T, typename INTERMEDIATE_T>
BASE_T
StandardDeviationTrue(const BASE_T* data, const BASE_T mean, unsigned int size)
{
  return SQRT(VarianceTrue(data, mean, size));
}
