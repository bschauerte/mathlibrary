/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

#include <string>
#include <string.h> // memcpy

#include "Math/Common.h"

/**
 * @class CombinationGenerator
 *
 * Create all combinations (n choose k).
 * You can use the CombinationGenerator to generate combinations of any data, by
 * using the generated combinations of indices to access an array holding the
 * elements of data which combination you need.
 *
 * @author Boris Schauerte
 * @author Michael Gilleland (original written in java)
 * @date 2009
 */
class CombinationGenerator
{
public:
  CombinationGenerator(unsigned int _n, unsigned int _r)
    : n(_n), r(_r)
  {
    numCombinations = Choose(n,r);
    numLeft = numCombinations;
    indices = new int[r];
    Reset();
  }
  ~CombinationGenerator(void)
  {
    delete [] indices;
  }

  /** Reset combination generator. */
  inline void Reset(void)
  {
    for (unsigned int i = 0; i < r; i++)
      indices[i] = i;
    numLeft = numCombinations;
  }
  /** Generate next combination. */
  inline const int* GetNext(void)
  {
    Next();
    return GetIndices();
  }
  /** Get pointer to the indices storage */
  inline const int* GetIndices(void)
  {
    return indices;
  }
  /** Get copy of the current indices */
  inline void GetIndices(int* _indices) const
  {
    memcpy((void*)_indices, (void*)indices, sizeof(int) * r);
  }
  /** Number of combinations left. */
  inline unsigned int NumLeft(void)
  {
    return numLeft;
  }
protected:
  /** Calculate next combination, according to:
   * H. Rosen, Discrete Mathematics and Its Applications, 2nd edition (NY: McGraw-Hill, 1991), pp. 284-286.
   */
  inline void Next(void)
  {
    if (numLeft == numCombinations)
    {
      numLeft--;
      return;
    }

    int i = r - 1;
    while (indices[i] == n - r + i)
      i--;

    indices[i] = indices[i] + 1;
    for (unsigned int j = i + 1; j < r; j++)
      indices[j] = indices[i] + j - i;

    --numLeft;
  }
private:
  unsigned int n, r;
  int* indices;
  unsigned int numCombinations;
  unsigned int numLeft;
};
