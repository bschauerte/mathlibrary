/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

/** @file Clipping2D.h
 *
 * Implementation of 2D Clipping algorithm (clipping against a window).
 * Clipping algorithm is based on the Sutherland-Hodgman algorithm
 * (http://en.wikipedia.org/wiki/Sutherland-Hodgman_clipping_algorithm).
 *
 * used resources:
 * - http://www.cs.helsinki.fi/group/goa/viewing/leikkaus/intro2.html
 * - http://www.cc.gatech.edu/grads/h/Hao-wei.Hsieh/Haowei.Hsieh/code2.html
 *
 * Note:
 * - this implementation is currently NOT optimized for speed!
 *
 * @author Boris Schauerte
 * @date 2009
 */

#include <vector>

#include "Math/Vector2D.h"

/** Clip the 2-D points against the given window. */
template <typename T, typename S> std::vector< Point2D<T> > ClipPolygon2D(const std::vector< Point2D<T> >& p, const Box2D<S>& window);
/** Clip the 2-D points against the given window. */
template <typename T, typename S> std::vector< Point2D<T> > ClipPolygon2D(const Point2D<T>* p, unsigned int numPoints, const Box2D<S>& window);
