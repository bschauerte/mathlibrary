/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

/** @file Vector3D.h
 *
 *  This file contains the following classes ...
 * ... as a base class ...
 * - Vec3D<T>
 * ... and it's extensions
 * - Point3D<T>
 * - Vector3D<T>
 *
 * Additional type definitions
 *   Vector3D<float>   = Vector3Df
 *   Vector3D<double>  = Vector3Dd
 *   Point3D<float>    = Point3Df;
 *   Point3D<double>   = Point3Dd;
 *   Vec3D<float>      = Vec3Df;
 *   Vec3D<double>     = Vec3Dd;
 *
 *  @author B. Schauerte
 *  @date 2009
 */
#pragma once

#include <math.h>

#include "Math/Common.h"

#include "_ml_assert.h"

#include <iostream>
#include <string>

/* Why distinguish between vector and point?
 *   because in 3D geometrics with 4D vectors and 4x4 matrices
 * a vector is (a,b,c,1) and a point (a,b,c,0).
 * This makes sense, because e.g. you can not rotate a point
 */

/**
 * @class Vec3D
 *
 * Base class for Vector3D and Point3D.
 * 4D base representation to support working with 4x4 transformation matrices.
 *
 * @author Boris Schauerte
 * @date 2009
 */
// confused by "template <typename T>" vs "template <class T>" then this is
// a nice read http://blogs.msdn.com/slippman/archive/2004/08/11/212768.aspx
template <typename T>
struct Vec3D
{
public:
  void Set(T _x, T _y, T _z);
  template <typename S>
  void Set(const Vec3D<S>& v)
  {
    x = (T)v.x;
    y = (T)v.y;
    z = (T)v.z;
  }


  // All the below operations are 3D, i.e. the constant is not considered
  /** Length of the vector (uses euclidean metric) */
  T Length(void) const;
  /** Normalize the vector (i.e. after normalization Length=1.0) */
  void Normalize(void);
  /** Provide array access */
  T& operator[](int i);
  /** Provide array access */
  const T& operator[](int i) const;
  /** Scaling */
  Vec3D& operator*=(T s);
  Vec3D& operator/=(T s);
  /** aka. *= -1.0 */
  Vec3D& operator-(void);
  /** Assignment operator */
  void operator=(const Vec3D<T>& source)
  {
    x = (T)source.x;
    y = (T)source.y;
    z = (T)source.z;
    c = (T)source.c;
  }

  /** Absolute value (square of the length) of the vector. */
  inline T Abs(void) const
  {
    return SQR(x) + SQR(y) + SQR(z);
  }

  /** Dot-product. */
  inline T DotProduct(const Vec3D<T>& v) const
  {
    return (x*v.x) + (y*v.y) + (z*v.z);
  }

  /** Calculate the p-norm. */
  inline T NormP(unsigned int p) const
  {
    _ML_ASSERT(p > (T)1.0);
    return pow(pow(fabs(x), (int)p) + pow(fabs(y), (int)p) + pow(fabs(z), (int)p), (T)1.0 / (T)p);
  }
  /** Calculate the manhattan-norm. */
  inline T NormManhattan(void) const
  {
    return fabs(x) + fabs(y) + fabs(z);
  }

  /** Element-wise multiplication. The constant c will be ignored. */
  inline void ElemMul(const Vec3D<T>& source)
  {
    x *= source.x;
    y *= source.y;
    z *= source.z;
  }
  /** Element-wise division. The constant c will be ignored. */
  inline void ElemDiv(const Vec3D<T>& source)
  {
    x /= source.x;
    y /= source.y;
    z /= source.z;
  }
  /** Calculate the max. of x, y and z. */
  inline T& ElemMax(void) { return MAX(x, MAX(y, z)); }
  /** Calculate the max. of x, y and z. */
  inline const T& ElemMax(void) const { return MAX(x, MAX(y, z)); }
  /** Calculate the min. of x, y and z. */
  inline T& ElemMin(void) { return MIN(x, MIN(y, z)); }
  /** Calculate the min. of x, y and z. */
  inline const T& ElemMin(void) const { return MIN(x, MIN(y, z)); }
  /** Element-wise absolute value operation. */
  inline const void ElemAbs(void)
  {
    x = fabs(x);
    y = fabs(y);
    z = fabs(z);
  }
  /** Element-wise round operation. Uses floor (next lower number). */
  inline const void ElemRoundDown(void)
  {
    x = floor(x);
    y = floor(y);
    z = floor(z);
  }
  /** Element-wise round operation. Uses ceil (next higher number). */
  inline const void ElemRoundUp(void)
  {
    x = ceil(x);
    y = ceil(y);
    z = ceil(z);
  }

  /** Rotation around the x-axis (right handed coord. system) */
  void RotateX(T angle);
  /** Rotation around the y-axis (right handed coord. system) */
  void RotateY(T angle);
  /** Rotation around the z-axis (right handed coord. system) */
  void RotateZ(T angle);

  /** Convert to string. */
  std::string ToString(void) const;

  /*T &x, &y, &z;      // 3D coordinates
  T &c;              // additional constant for 4x4 transformation matrices
  T data[4];         // x,y,z,c as array*/
  T x, y, z;
  T c;
protected:
  Vec3D(void);
  Vec3D(T _x, T _y, T _z, T _c);
};

template <typename T> std::ostream& operator<<(std::ostream &os, const Vec3D<T>& v);
template <typename T> std::istream& operator>>(std::istream &is, Vec3D<T>& v);

/**
 * @class Vector3D
 *
 * Representation of a vector in 3D-space.
 *
 * @author Boris Schauerte
 * @date 2009
 */
template <typename T>
struct Vector3D
  : public Vec3D<T>
{
public:
  //template <typename S>
  //void Set(const Vector3D<S>& v)
  //{
  //  Vec3D<T>::x = (T)v.x;
  //  Vec3D<T>::y = (T)v.y;
  //  Vec3D<T>::z = (T)v.z;
  //}

  Vector3D(void);
  Vector3D(T _x, T _y, T _z);
  Vector3D(const Vector3D& v);
};

/**
 * @class Point3D
 *
 * Representation of a point in 3D-space.
 *
 * @author Boris Schauerte
 */
template <typename T>
struct Point3D
  : public Vec3D<T>
{
public:
  Point3D(void);
  Point3D(T _x, T _y, T _z);
  Point3D(const Point3D& p);

  //template <typename S>
  //void Set(const Point3D<S>& p)
  //{
  //  Vec3D<T>::x = (T)p.x;
  //  Vec3D<T>::y = (T)p.y;
  //  Vec3D<T>::z = (T)p.z;
  //}

  inline void operator+=(const Vector3D<T>& v)
  {
    Vec3D<T>::x += v.x;
    Vec3D<T>::y += v.y;
    Vec3D<T>::z += v.z;
  }
  inline void operator-=(const Vector3D<T>& v)
  {
    Vec3D<T>::x -= v.x;
    Vec3D<T>::y -= v.y;
    Vec3D<T>::z -= v.z;
  }
};

/* Cross-product of vector u and v.
 * Calculates the normal of the plane defined by u and v.
 */
template <typename T> Vector3D<T> CrossProduct(const Vector3D<T>& u, const Vector3D<T>& v);
/* Calculate the DotProduct of the vectors. */
template <typename T> T DotProduct(const Vector3D<T>& u, const Vector3D<T>& v);

/* Scaling operators */
template <typename T> Vector3D<T> operator*(const Vector3D<T>& v, T s);
template <typename T> Vector3D<T> operator*(T s, const Vector3D<T>& v);
template <typename T> Vector3D<T> operator*(const Vector3D<T>& v, unsigned int i);
template <typename T> Vector3D<T> operator*(unsigned int i, const Vector3D<T>& v);

/* Vector/Point operators */
// point + vector results in a point
// point +/- point results in a vector
// vector +/- vector results in a vector
template <typename T> Point3D<T> operator+(const Point3D<T>& p, const Vector3D<T>& v);
template <typename T> Point3D<T> operator-(const Point3D<T>& p, const Vector3D<T>& v);
template <typename T> Vector3D<T> operator+(const Vector3D<T>& v1, const Vector3D<T>& v2);
template <typename T> Vector3D<T> operator-(const Vector3D<T>& v1, const Vector3D<T>& v2);
template <typename T> Vector3D<T> operator+(const Point3D<T>& p1, const Point3D<T>& p2);
template <typename T> Vector3D<T> operator-(const Point3D<T>& p1, const Point3D<T>& p2);

/* Additional type definitions */
typedef Vector3D<float> Vector3Df;
typedef Vector3D<double> Vector3Dd;
typedef Point3D<float> Point3Df;
typedef Point3D<double> Point3Dd;
typedef Vec3D<float> Vec3Df;
typedef Vec3D<double> Vec3Dd;

/* Convert between precision */
//template <typename T, typename S> Point3D<T> operator=(const Point3D<T>& p1, const Point3D<S>& p2);
//template <typename T, typename S> Vector3D<T> operator=(const Vector3D<T>& p1, const Vector3D<S>& p2);

#define __INSTANTIATE_OPERATORS(_TYPENAME) template std::ostream& operator<<(std::ostream &os,const Vec3D<_TYPENAME> &v); \
  template std::istream& operator>>(std::istream &is,Vec3D<_TYPENAME> &v); \
  template Vector3D<_TYPENAME> CrossProduct(const Vector3D<_TYPENAME>& u, const Vector3D<_TYPENAME>& v); \
  template _TYPENAME DotProduct(const Vector3D<_TYPENAME>& u, const Vector3D<_TYPENAME>& v); \
  template Vector3D<_TYPENAME> operator*(const Vector3D<_TYPENAME>& v, _TYPENAME s); \
  template Vector3D<_TYPENAME> operator*(_TYPENAME s, const Vector3D<_TYPENAME>& v); \
  template Vector3D<_TYPENAME> operator*(const Vector3D<_TYPENAME>& v, unsigned int i); \
  template Vector3D<_TYPENAME> operator*(unsigned int i, const Vector3D<_TYPENAME>& v); \
  template Point3D<_TYPENAME> operator+(const Point3D<_TYPENAME>& p, const Vector3D<_TYPENAME>& v); \
  template Point3D<_TYPENAME> operator-(const Point3D<_TYPENAME>& p, const Vector3D<_TYPENAME>& v); \
  template Vector3D<_TYPENAME> operator+(const Vector3D<_TYPENAME>& v1, const Vector3D<_TYPENAME>& v2); \
  template Vector3D<_TYPENAME> operator-(const Vector3D<_TYPENAME>& v1, const Vector3D<_TYPENAME>& v2); \
  template Vector3D<_TYPENAME> operator+(const Point3D<_TYPENAME>& p1, const Point3D<_TYPENAME>& p2); \
  template Vector3D<_TYPENAME> operator-(const Point3D<_TYPENAME>& p1, const Point3D<_TYPENAME>& p2);

/**
 * Box3D
 *
 * Helper data structure to describe a 3-dimensional box.
 * For correct functionality it is necessary that high.x >= low.x && high.y >= low.y && high.z >= low.z.
 *
 * @author Boris Schauerte
 * @date 2009
 */
template <typename T>
struct Box3D
{
public:
  Box3D(void)
  {
  }
  Box3D(const Point3D<T>& p1, const Point3D<T>& p2)
  : low(p1), high(p2)
  {
  }

  /** Calculate the volume of the 3D box. */
  inline T Volume(void) const { return (high.x - low.x)*(high.y - low.y)*(high.z - low.z); }
  /** Is the point inside the box? */
  inline bool IsInside(const Point3Df& p) const
  {
    return ((low.x <= p.x && p.x <= high.x) &&
        (low.y <= p.y && p.y <= high.y) &&
        (low.z <= p.z && p.z <= high.z));
  }
  /** Specify the box with a low and high point (low <= high). */
  inline void Set(const Point3D<T>& _low, const Point3D<T>& _high)
  {
    low = _low;
    high = _high;
  }

  Point3D<T> low;
  Point3D<T> high;
protected:
private:
};
typedef Box3D<float> Box3Df;
typedef Box3D<double> Box3Dd;
