/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

//#define _USE_DISJOINT_SETS_VECTOR
//#define _USE_DISJOINT_SETS_SSE

#ifdef _USE_DISJOINT_SETS_VECTOR
#include <vector>
#else
#ifdef _USE_DISJOINT_SETS_SSE
#include "Performance/SIMD/AlignedData.h"
#endif
#endif

#include "_ml_assert.h"

#include <cstring>
#include <cstdlib>

#define _DEFAULT_DISJOINT_SETS_CAPACITY (1024)
#define _DEFAULT_CAPACITY_RESIZE_DELTA  (1024)

/**
 * @class DisjointSets
 *
 * Implementation of a disjoint-set data structure.
 *
 * Sets are numbered from [1..n]. 0 is used to inidicate that the element is not
 * initialized (unknown parent/set).
 *
 * @author Boris Schauerte
 * @date 2009
 */
class DisjointSets
{
public:
#ifdef _USE_DISJOINT_SETS_VECTOR
  typedef std::vector<unsigned int> ParentsContainer;
#else
  typedef unsigned int* ParentsContainer;
#endif

  DisjointSets(void)
#ifndef _USE_DISJOINT_SETS_VECTOR
    : parents(NULL), ranks(NULL),
#else
    : parents(_DEFAULT_DISJOINT_SETS_CAPACITY, 0), ranks(_DEFAULT_DISJOINT_SETS_CAPACITY, 0),
#endif
    size(0), capacity(_DEFAULT_DISJOINT_SETS_CAPACITY)
  {
#ifndef _USE_DISJOINT_SETS_VECTOR
#ifdef _USE_DISJOINT_SETS_SSE
    parents = (unsigned int*)AlignedMalloc(sizeof(unsigned int) * capacity, _SSE_ALIGNMENT);
    ranks = (unsigned int*)AlignedMalloc(sizeof(unsigned int) * capacity, _SSE_ALIGNMENT);
#else
    parents = (unsigned int*)malloc(sizeof(unsigned int) * capacity);
    ranks = (unsigned int*)malloc(sizeof(unsigned int) * capacity);
#endif
    /* ... and set the new tail/elements to zero */
    memset((void*)parents, 0, sizeof(unsigned int) * capacity);
    memset((void*)ranks, 0, sizeof(unsigned int) * capacity);
#endif
  }
  ~DisjointSets(void)
  {
    Free();
  }

  /* Basic union-find operations */
  /* --------------------------- */
  /** Make a new set. */
  inline void MakeSet(unsigned int x)
  {
    if (x > size)
    {
      size = x;
      if (size+1 > capacity)
      {
        // make space for more elements
        Resize(size + _DEFAULT_CAPACITY_RESIZE_DELTA);
      }
    }
    parents[x] = x;
    ranks[x] = 0; // @note : this is not necessary, because vector is initialized with zero
  }
  /** Union the sets to which x and y belong. */
  inline void UnionFind(unsigned int x, unsigned int y)
  {
    _ML_ASSERT(IsInitialized(x));
    _ML_ASSERT(IsInitialized(y));

    unsigned int rep1 = Find(x);
    unsigned int rep2 = Find(y);
    Union(rep1, rep2);
  }
  /** Union the sets represented by rep1 and rep2. */
  inline void Union(unsigned int rep1, unsigned int rep2)
  {
    _ML_ASSERT(IsInitialized(rep1));
    _ML_ASSERT(IsInitialized(rep2));

    parents[rep1] = rep2;
  }
  /** Union the sets to which x and y belong. Use union by rank. */
  inline void UnionFindByRank(unsigned int x, unsigned int y)
  {
    _ML_ASSERT(IsInitialized(x));
    _ML_ASSERT(IsInitialized(y));

    unsigned int rep1 = Find(x);
    unsigned int rep2 = Find(y);
    UnionByRank(rep1, rep2);
  }
  /** Union the sets represented by rep1 and rep2. Use union by rank. */
  inline void UnionByRank(unsigned int rep1, unsigned int rep2)
  {
    _ML_ASSERT(IsInitialized(rep1));
    _ML_ASSERT(IsInitialized(rep2));

    if (ranks[rep1] > ranks[rep2])
    {
      parents[rep2] = rep1;
    }
    else if (ranks[rep1] < ranks[rep2])
    {
      parents[rep1] = rep2;
    }
    else if (rep1 != rep2)
    {
      parents[rep2] = rep1;
      ranks[rep1] += 1;
    }
  }
  /** Directly set parent+child relation. */
  inline void Set(unsigned int parent, unsigned int child)
  {
    _ML_ASSERT(IsInitialized(parent));
    _ML_ASSERT(IsInitialized(child));

    parents[child] = parent;
  }
  /** Find the representative of x. */
  unsigned int Find(unsigned int x) const
  {
    _ML_ASSERT(IsInitialized(x));

    unsigned int parent = parents[x];
    while (parent != x)
    {
      x = parent;
      parent = parents[x];
    }
    return parent;
  }

  /* Additional functionality */
  /* ------------------------ */
  /** Find - modified to use path compression. */
  unsigned int FindPathCompression(unsigned int x)
  {
    _ML_ASSERT(IsInitialized(x));

    if (parents[x] == x)
    {
      return x;
    }
    else
    {
      parents[x] = FindPathCompression(parents[x]);
      return parents[x];
    }
  }
  /** After flatten we find the representative with O(1). */
  void Flatten(void)
  {
    for (size_t i = 1; i <= size; i++)
      parents[i] = Find(parents[i]);
#ifdef _USE_DISJOINT_SETS_VECTOR
    ranks.assign(size, 0); COMPILE ERROR, NOTE TESTING NEEDED // @TODO: test
#else
    memset((void*)ranks, 0, sizeof(unsigned int) * size);
#endif
  }
  /** Count the number of parents/sets. */
  size_t CountSets(void) const
  {
    size_t count = 0;
    for (unsigned int i = 1; i <= size; i++)
      if (IsRepresentative(i))
        ++count;
    return count;
  }
  /** Is x a set representative? */
  inline bool IsRepresentative(unsigned int x) const
  {
    return (parents[x] == x);
  }
  /** Is x initialized? */
  inline bool IsInitialized(unsigned int x) const
  {
    return (parents[x] != 0);
  }

  /** Return number of elements. */
  inline size_t Size(void) const
  {
    return size;
  }
  /** Return the parents. Be careful if you use this directly (0 is special case,...). */
  const ParentsContainer& GetParents(void) const
  {
    return parents;
  }

  /* Some statistics, e.g. manual tuning */
  /* ------------------------ */
  /** Find the max. depth (if viewed as forest of trees) */
  size_t MaxDepth(void) const
  {
    size_t maxDepth = 0;
    for (unsigned int i = 1; i <= size; i++)
    {
      size_t depth = 0;
      unsigned int idx = i;
      while (parents[idx] != idx)
      {
        idx = parents[idx];
        ++depth;
      }
      if (depth > maxDepth)
        maxDepth = depth;
    }
    return maxDepth;
  }
protected:
private:
  inline void Free(void)
  {
#ifndef _USE_DISJOINT_SETS_VECTOR
    if (parents != NULL)
#ifdef _USE_DISJOINT_SETS_SSE
      AlignedFree(parents);
#else
      free((void*)parents);
#endif
    if (ranks != NULL)
#ifdef _USE_DISJOINT_SETS_SSE
      AlignedFree(ranks);
#else
      free((void*)ranks);
#endif
#endif
  }

  inline void Resize(unsigned int _capacity)
  {
    _ML_ASSERT(_capacity > capacity); // only resize to larger sizes

#ifdef _USE_DISJOINT_SETS_VECTOR
    parents.resize(_capacity, 0); // use value=0 as special value => not initialized!
    ranks.resize(_capacity, 0);
#else
    /* allocate new arrays */
#ifdef _USE_DISJOINT_SETS_SSE
    unsigned int* _parents = (unsigned int*)AlignedMalloc(sizeof(unsigned int) * _capacity, _SSE_ALIGNMENT);
    unsigned int* _ranks = (unsigned int*)AlignedMalloc(sizeof(unsigned int) * _capacity, _SSE_ALIGNMENT);
#else
    unsigned int* _parents = (unsigned int*)malloc(sizeof(unsigned int) * _capacity);
    unsigned int* _ranks = (unsigned int*)malloc(sizeof(unsigned int) * _capacity);
#endif
    _ML_ASSERT(_ranks != NULL);
    _ML_ASSERT(_parents != NULL);
    /* copy values into new arrays ... */
    memcpy((void*)_parents, (void*)parents, sizeof(unsigned int) * capacity);
    memcpy((void*)_ranks, (void*)ranks, sizeof(unsigned int) * capacity);
    /* ... and set the new tail/elements to zero */
    memset((void*)&_parents[capacity], 0, sizeof(unsigned int) * (_capacity - capacity));
    memset((void*)&_ranks[capacity], 0, sizeof(unsigned int) * (_capacity - capacity));

    /* Delete old arrays */
    Free();

    parents = _parents;
    ranks = _ranks;
#endif
    capacity = _capacity;
  }

  ParentsContainer parents;           // parents
#ifdef _USE_DISJOINT_SETS_VECTOR
  std::vector<unsigned int> ranks;    // ranks - to support "union by rank"
#else
  unsigned int* ranks;                // ranks - to support "union by rank"
#endif
  unsigned int size;                  // number of elements
  unsigned int capacity;              // max. number of elements
};
