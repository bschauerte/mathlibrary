/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

#include "_ml_assert.h"

#include <float.h>

/** @file Polynom.h
 *
 * Evaluate a polyon.
 *
 * @author Boris Schauerte
 * @date 2009
 */

/**
 * Y = POLYVAL(P,X)
 * returns the value of a polynomial P evaluated at X. P is
 * a vector of length N+1 whose elements are the coefficients
 * of the polynomial in descending powers.
 *      Y = P(1)*X^N + P(2)*X^(N-1) + ... + P(N)*X + P(N+1)
 */
template <typename T, typename S>
inline
T Polyval(const T& x, const T* coeffs, const S n) // n = polynom degree => number of coefficients is n+1
{
  T y(0);
  for (S i = 0; i <= n; i++)
  //for (S i = n; i >= 0; --i)
  {
    y += coeffs[n - i] * pow(x,i);
  }
  return y;
}

/**
 * ..
 */
template <typename T, typename S>
inline
T PolyFindSample(const T& y,
    const T* coeffs,
    const S n, // n = polynom degree => number of coefficients is n+1
    const T xmin = 0.0,
    const T xmax = 1.0,
    const T step = 0.01
    )
{
  double minError = FLT_MAX;
  double optX = 0;
  for (double x = 0; x <= 1.0; x += step)
  {
    double _y = Polyval(x,coeffs,n);
    if (fabs(_y - y) < minError)
    {
      optX = x;
      minError = fabs(_y - y);
    }
  }
  return optX;
}

/**
 * Y = POLYVAL(P,X)
 * returns the value of a polynomial P evaluated at X. P is
 * a vector of length N+1 whose elements are the coefficients
 * of the polynomial in descending powers.
 *      Y = P(1)*X^N + P(2)*X^(N-1) + ... + P(N)*X + P(N+1)
 */
template <typename T, typename S>
inline
T* Polytable(
    const T* x,
    T* y,
    const S numel,
    const T* coeffs,
    const S n) // n = polynom degree => number of coefficients is n+1
{
  // allocate memory if necessary
  if (y == 0)
  //  y = new T[numel];
    _ML_ASSERT(y != NULL);

  // evaluate the polynom for all elements of x
  for (S e = 0; e < numel; e++)
  {
    const T _x(x[e]);
    T _y(0);
    // calculate the value of the polynom at x[e]
    for (S i = 0; i <= n; i++)
      _y += coeffs[n - i] * pow(_x,i);
    y[e] = _y;
  }
  return y;
}

/**
 * Y = POLYVAL(P,X)
 * returns the value of a polynomial P evaluated at X.
 */
template <typename T, typename S>
inline
T Polyval(const T& x, const T* coeffs, const T* exps, const S n)
{
  T y(0);
  for (S i = 0; i < n; i++)
  {
    y += coeffs[i] * pow(x,exps[i]);
  }
  return y;
}

