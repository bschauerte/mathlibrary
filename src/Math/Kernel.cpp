#include "_ml_base_def.h"
#include "Kernel.h"

#include <iostream>
#include <cmath>

/********************************* CLASS KERNEL ********************************/
Kernel::Kernel(void)
{
	m_kernelfunction = KERNEL_UNDEFINED;
	m_dimensions = -1;
	m_kernelvalues = 0;
	m_resolutions = 0;
	m_bandwidth = 0;
	m_kernelsize = 0;
}

Kernel::~Kernel(void)
{
	Clear();
}

void Kernel::Init(float* bandwidth, int dimensions, int kernel)
{
	Clear();
	m_kernelfunction = kernel;
	m_dimensions = dimensions;
	float maxb = 0.0;
	for (int i=0; i<dimensions; i++)
	{
		if (bandwidth[i] > maxb)	maxb = bandwidth[i];
	}
	m_bandwidth = maxb;

	if (m_kerneltype == KERNEL_TYPE_MULTIVARIATE)
	{
		m_resolutions = new float[dimensions];
		for (int i=0; i<dimensions; i++)
		{
			m_resolutions[i] = bandwidth[i]/m_bandwidth;
		}
	}

	if (kernel == KERNEL_GAUSSIAN)
		m_kernelsize = static_cast<int>(ceil(3*m_bandwidth));
	else
		m_kernelsize = static_cast<int>(ceil(3*m_bandwidth));

	m_kernelvalues = new float[m_kernelsize];
	Precompute();
}

void Kernel::Init(float bandwidth, int dimensions, int kernel)
{
	Clear();
	m_kernelfunction = kernel;
	m_dimensions = dimensions;
	m_bandwidth = bandwidth;

	if (m_kerneltype == KERNEL_TYPE_MULTIVARIATE)
	{
		m_resolutions = new float[dimensions];
		for (int i=0; i<dimensions; i++)
		{
			m_resolutions[i] = 1.0;
		}
	}

	if (kernel == KERNEL_GAUSSIAN)
		m_kernelsize = static_cast<int>(ceil(3*m_bandwidth));
	else
		m_kernelsize = static_cast<int>(ceil(3*m_bandwidth));

	m_kernelvalues = new float[m_kernelsize];
	Precompute();
}

void Kernel::Precompute(void)
{
	double t = 0.0;
	if (m_kernelvalues && (m_kernelsize > 0))
	{
		for (int j=0; j<m_kernelsize; j++)
		{
			if (m_kernelfunction == KERNEL_UNIFORM)
				m_kernelvalues[j] = 1.0;
			else if (m_kernelfunction == KERNEL_GAUSSIAN)
			{
				t = j/m_bandwidth;
				m_kernelvalues[j] = exp(-0.5*t*t);
			}
			else if (m_kernelfunction == KERNEL_LINEAR)
			{
				m_kernelvalues[j] = 1-j/m_kernelsize;
			}
			else if (m_kernelfunction == KERNEL_EPANECHNIKOV)
			{
				m_kernelvalues[j] = 1-j*j/(m_kernelsize*m_kernelsize);
			}
			else
			{
				std::cerr << "<KERNEL>: WARNING: Unknown kernel type! Using uniform kernel." << std::endl;
				m_kernelvalues[j] = 1.0;
			}
		}
	}
}

int Kernel::Size(int idx)
{
	if (m_kerneltype == KERNEL_TYPE_MULTIVARIATE)
		return static_cast<int>(m_kernelsize*m_resolutions[idx]);

	return m_kernelsize;
}

void Kernel::Clear(void)
{
	if (m_kernelvalues)	delete[] m_kernelvalues;
	if (m_resolutions)	delete[] m_resolutions;
	m_kernelvalues = 0;
	m_resolutions = 0;
	m_kernelsize = 0;
	m_bandwidth = 0.0;
	m_dimensions = 0;
}

/********************************* CLASS KERNELMULTIVARIATE *******************************/

KernelMultivariate::KernelMultivariate(void) : Kernel()
{
	m_kerneltype = KERNEL_TYPE_MULTIVARIATE;
}

KernelMultivariate::~KernelMultivariate(void)
{
	Clear();
}

double KernelMultivariate::GetWeighting(VectorND<double> diff, double* resolution)
{
	if (!resolution)	return 0.0;
	double ret = 1.0;
	int idx = 0;
	for (int i=0; i<m_dimensions; i++)
	{
		idx = abs(static_cast<int>(floor(diff[i]/resolution[i]*m_resolutions[i])));
		if (idx >= m_kernelsize)	return 0.0;
		ret *= m_kernelvalues[idx];
	}
	return ret;
}

double KernelMultivariate::GetWeighting(VectorND<double> center, VectorND<double> data, double* resolution)
{
	if (!resolution)	return 0.0;
	double ret = 1.0;
	int idx = 0;
	for (int i=0; i<m_dimensions; i++)
	{
		idx = abs(static_cast<int>(floor((data[i]-center[i])/resolution[i]*m_resolutions[i])));
		if (idx >= m_kernelsize)	return 0.0;
		ret *= m_kernelvalues[idx];
	}
	return ret;
}

double KernelMultivariate::GetWeighting(VectorND<double> diff, double resolution)
{
	double ret = 1.0;
	int idx = 0;
	for (int i=0; i<m_dimensions; i++)
	{
		idx = abs(static_cast<int>(floor(diff[i]/resolution*m_resolutions[i])));
		if (idx >= m_kernelsize)	return 0.0;
		ret *= m_kernelvalues[idx];
	}
	return ret;
}

double KernelMultivariate::GetWeighting(VectorND<double> center, VectorND<double> data, double resolution)
{
	double ret = 1.0;
	int idx = 0;
	for (int i=0; i<m_dimensions; i++)
	{
		idx = abs(static_cast<int>(floor((data[i]-center[i])/resolution*m_resolutions[i])));
		if (idx >= m_kernelsize)	return 0.0;
		ret *= m_kernelvalues[idx];
	}
	return ret;
}

Kernel* KernelMultivariate::GetDerivative(void)
{
	KernelMultivariate* ret = new KernelMultivariate;
	int kt = KERNEL_UNDEFINED;

	if (m_kernelfunction == KERNEL_GAUSSIAN)
		kt = KERNEL_GAUSSIAN;
	else if (m_kernelfunction == KERNEL_LINEAR)
		kt = KERNEL_UNIFORM;
	else if (m_kernelfunction == KERNEL_EPANECHNIKOV)
		kt = KERNEL_LINEAR;
	else
	{
		std::cerr << "<Kernel> WARNING: Requested derivative of unknown kernel type or uniform kernel (which is 0)." << std::endl;
	}

	float* bw = new float[m_dimensions];
	for (int i=0; i<m_dimensions; i++)
		bw[i] = m_bandwidth*m_resolutions[i];
	ret->Init(bw, m_dimensions, kt);
	delete[] bw;
	return ret;
}

/********************************* CLASS KERNELROTATIONAL *******************************/

KernelRotational::KernelRotational(void) : Kernel()
{
	m_kerneltype = KERNEL_TYPE_ROTATIONAL;
}

KernelRotational::~KernelRotational(void)
{
	Clear();
}

double KernelRotational::GetWeighting(VectorND<double> diff, double* resolution)
{
	if (!resolution)	return 0.0;
	double ret = 1.0;
	int idx = 0;
	for (int i=0; i<m_dimensions; i++)
	{
		idx = abs(static_cast<int>(floor(diff[i]/resolution[i])));
		if (idx >= m_kernelsize)	return 0.0;
		ret *= m_kernelvalues[idx];
	}
	return ret;
}

double KernelRotational::GetWeighting(VectorND<double> center, VectorND<double> data, double* resolution)
{
	if (!resolution)	return 0.0;
	double ret = 1.0;
	int idx = 0;
	for (int i=0; i<m_dimensions; i++)
	{
		idx = abs(static_cast<int>(floor((data[i]-center[i])/resolution[i])));
		if (idx >= m_kernelsize)	return 0.0;
		ret *= m_kernelvalues[idx];
	}
	return ret;
}

double KernelRotational::GetWeighting(VectorND<double> diff, double resolution)
{
	double ret = 1.0;
	int idx = 0;
	for (int i=0; i<m_dimensions; i++)
	{
		idx = abs(static_cast<int>(floor(diff[i]/resolution)));
		if (idx >= m_kernelsize)	return 0.0;
		ret *= m_kernelvalues[idx];
	}
	return ret;
}

double KernelRotational::GetWeighting(VectorND<double> center, VectorND<double> data, double resolution)
{
	double ret = 1.0;
	int idx = 0;
	for (int i=0; i<m_dimensions; i++)
	{
		idx = abs(static_cast<int>(floor((data[i]-center[i])/resolution)));
		if (idx >= m_kernelsize)	return 0.0;
		ret *= m_kernelvalues[idx];
	}
	return ret;
}

Kernel* KernelRotational::GetDerivative(void)
{
	KernelRotational* ret = new KernelRotational;
	int kt = KERNEL_UNDEFINED;

	if (m_kernelfunction == KERNEL_GAUSSIAN)
		kt = KERNEL_GAUSSIAN;
	else if (m_kernelfunction == KERNEL_LINEAR)
		kt = KERNEL_UNIFORM;
	else if (m_kernelfunction == KERNEL_EPANECHNIKOV)
		kt = KERNEL_LINEAR;
	else
	{
		std::cerr << "<Kernel> WARNING: Requested derivative of unknown kernel type or uniform kernel (which is 0)." << std::endl;
	}

	ret->Init(m_bandwidth, m_dimensions, kt);
	return ret;
}
