/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

/** @file Geometry.h
 *
 * This file contains methods and classes ...
 * ... for 2D & 3D geometry
 *
 * @author B. Schauerte
 * @date 2009
 */
#pragma once

#include "Math/Common.h"
#include "Math/Vector3D.h"
#include "Math/Vector2D.h"

#include "_ml_assert.h"

/* Calculate the (min.) distance between a point and a line.
 */
template <typename T>
inline T
DistancePointLine(const Point3D<T>& point, const Point3D<T>& linePoint, const Vector3D<T>& lineDirection)
{
  // P is the point, g is the direction-vector of the line and A is a point on the line
  // d is the distance
  // d = |(P-A) x g|/|g| // x means CrossProduct
  Vector3D<T> PA = point - linePoint;
  Vector3D<T> crossProduct = CrossProduct(PA, lineDirection);
  return (crossProduct.Length()) / (lineDirection.Length());
}

/* tests if a point is Left|On|Right of an infinite line.
 *    Input:  three points P0, P1, and P2
 *    Return: >0 for P2 left of the line through P0 and P1
 *            =0 for P2 on the line
 *            <0 for P2 right of the line
 */
// NOTE: Abused 3D points/vectors for 2D geometry (ugly)
template <typename T>
inline float
IsLeft(const Point3D<T>& p0, const Point3D<T>& p1, const Point3D<T>& p2)
{
  return ( (p1.x - p0.x)*(p2.y - p0.y) - (p2.x - p0.x)*(p1.y - p0.y) );
}

/* tests if a point is Left|On|Right of an infinite line.
 * @return    >0 if P2 left    of the (infinite) line through P0 and P1
 *            =0 if P2 on      the line
 *            <0 if P2 right   of the line
 */
template <typename T>
inline double
IsLeft(const Point2D<T>& p0, const Point2D<T>& p1, const Point2D<T>& p2)
{
  return ( (p1.x - p0.x)*(p2.y - p0.y) - (p2.x - p0.x)*(p1.y - p0.y) );
}

/* For convex polygon in Clockwise (CW) order (polygon needs to be convex).
 * Only for the x-y-Plane. The z-component is ignored.
 */
// NOTE: Abused 3D points/vectors for 2D geometry (ugly)
template <typename T>
inline bool
PointInsideConvexPolygon2D(const Point3D<T>& p, const Point3D<T>* polygon, const size_t numPoints)
{
  Vector3D<T> u, v;
  for (size_t i = 0; i < numPoints; i++)
  {
    char signum = SGN(IsLeft(polygon[i], polygon[(i+1) % numPoints], p));
    switch(signum)
    {
    case 0: // p lies exactly on the line p[i]->p[i+1]
      break;
    case 1: // p lies on the "left" side of the line p[i]->p[i+1] (positive angle)
      break;
    case -1: // p lies on the "right" side of the line p[i]->p[i+1] (negative angle)
      return false;
      break;
    default:
      _ML_ASSERT(false);
    }
  }
  return true;
}

/* For convex polygon in Clockwise (CW) order (polygon needs to be convex).
 */
template <typename T>
inline bool
PointInsideConvexPolygon2D(const Point2D<T>& p, const Point2D<T>* polygon, const size_t numPoints)
{
  Vector2D<T> u, v;
  for (size_t i = 0; i < numPoints; i++)
  {
    char signum = SGN(IsLeft(polygon[i], polygon[(i+1) % numPoints], p));
    switch(signum)
    {
    case 0: // p lies exactly on the line p[i]->p[i+1]
      break;
    case 1: // p lies on the "left" side of the line p[i]->p[i+1] (positive angle)
      break;
    case -1: // p lies on the "right" side of the line p[i]->p[i+1] (negative angle)
      return false;
      break;
    default:
      _ML_ASSERT(false);
    }
  }
  return true;
}

/* Test is point p lies inside the polygon
 */
template <typename T>
inline bool
PointInsidePolygon2D(const Point2D<T>& p, const Point2D<T>* polygon, const size_t numPoints)
{
  size_t counter = 0;
  size_t i;
  T xinters;
  Point2D<T> p1, p2;

  p1 = polygon[0];
  for (i = 1; i <= numPoints; i++)
  {
    p2 = polygon[i % numPoints];
    if (p.y > MIN(p1.y,p2.y))
    {
      if (p.y <= MAX(p1.y,p2.y))
      {
        if (p.x <= MAX(p1.x,p2.x))
        {
          if (p1.y != p2.y)
          {
            xinters = (p.y-p1.y) * (p2.x-p1.x) / (p2.y - p1.y) + p1.x;
            if (p1.x == p2.x || p.x <= xinters)
              counter++;
          }
        }
      }
    }
    p1 = p2;
  }

  if (counter % 2 == 0)
    return false;
  else
    return true;
}

/* Calculate the pan/tilt so for a given ray/direction, reference coordinate system is the global coordinate system */
template <typename T>
inline void
GetPanTiltFromDirection(const Vector3D<T>& direction,
                        double& panTarget, double& tiltTarget)
{
  const Point3D<T> zero((T)0,(T)0,(T)0);
  Point3D<T> tmp;
  Point3D<T> relTarget = zero + direction;

  // calculate pan
  // -> directly project the ray into the (x,y)-plane -> keeps pan angle
  //    -> pan is the angle between the ray and the x-axis
  tmp.Set(relTarget.x, relTarget.y, 0);
  double cosPanTarget = tmp.x / tmp.Length();
  panTarget = acos(cosPanTarget) * (relTarget.y > 0 ? 1 : -1); // note: acos has output [0,PI]=[0,180] -> the sign is missing -> use SGN

  // calculate tilt
  // -> directly rotate the ray into the (x,z)-plane -> projections would destroy angle
  //    -> tilt is the angle between the ray and the x-axis
  tmp.Set(SQRT(SQR(relTarget.x) + SQR(relTarget.y)), 0, relTarget.z);
  double cosTiltTarget = tmp.x / tmp.Length();
  tiltTarget = acos(cosTiltTarget) * (relTarget.z < 0 ? 1 : -1);
}

/* Calculate the pan/tilt so that the ray from posCamera goes to posTarget. */
template <typename T>
inline void
GetPanTiltFromTargetPos(const Point3D<T>& posTarget, const Point3D<T>& posCamera,
                        double& panTarget, double& tiltTarget)
{
  const Vector3D<T> direction = posTarget - posCamera;
  GetPanTiltFromDirection(direction, panTarget, tiltTarget);
}

/* Calculate the direction from the pan/tilt. */
template <typename T>
inline void
GetCamDirectionFromPanTilt(double pan, double tilt, Vector3D<T>& camDirection)
{
  Vector3D<T> foo(1.0, 0.0, 0.0);
  foo.RotateY((T)tilt);
  foo.RotateZ((T)pan);
  camDirection = foo;
}

/*
 * Graham Scan-Algorithm, using a (simulated) stack, w/o interior elimination.
 * Complexity: O(N log N) [with quicksort], here O(N^1.5) [with shellsort]
 *
 * The code was designed for small point-sets, but it does support arbitrary large point sets.
 * If you want to speed it up for large point sets, you could start with the following changes:
 * - use interior elimination
 * - use another sorting algorithm (or at least use a better shellsort sequence with a LUT)
 * - in any case, try to reduce the high amount of vector swapping operations for sorting
 */
/**
 * Compute the Convex Hull of a 2D point-set. Please note that the code was designed
 * for a small amount of points.
 * Complexity:   O(n^1.5)
 * @param p     The point set. Note that the order of elements in p can change.
 * @param hull  The points of the convex hull will be stored in this
 *              array. They form a convex polygon (in CCW order).
 * @return      The number of points that form the convex hull.
 */
template <typename T, typename S> S CalculateConvexHull(Point2D<T> p[], Point2D<T> hull[], const S N);

/* Kay and Kayjia "slabs" algorithms for Ray-Box-Intersection test. */
template <typename T>
bool
IntersectsRayBox(const Point3D<T>& o, const Vector3D<T>& d, // origin and direction of the ray
                 const Box3D<T>& box);                      // the box to check against

/* Calculate the area of a simple polygon, based on the "Gauss'sche Trapezformel" */
template <typename T, typename S>
T
PolygonArea(const Point2D<T>* points, const S numPoints, bool unsignedArea = true);

/* Calculate the centroid of a simple polygon */
template <typename T, typename S>
Point2D<T>
PolygonCentroid(const Point2D<T>* points, const S numPoints);

