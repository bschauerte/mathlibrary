/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#include "_ml_base_def.h"
#include "Clipping2D.h"

enum eClippingWindowEdges
{
  eEdgeTop,
  eEdgeBottom,
  eEdgeLeft,
  eEdgeRight
};

template <typename T, typename S>
inline bool 
ClipPointInside(const Point2D<T>& p, const Box2D<S>& window, eClippingWindowEdges edge)
{
  switch (edge)
  {
  case eEdgeLeft:
    return p.x >= window.low.x;
  case eEdgeRight:
    return p.x <= window.high.x;
  case eEdgeTop:
    return p.y <= window.high.y;
  case eEdgeBottom:
    return p.y >= window.low.y;
  default:
    _ML_ASSERT(false);
    return false;
  }
}

template <typename T, typename S>
inline Point2D<T> 
ClipPointIntersect(const Point2D<T>& from, const Point2D<T>& to, const Box2D<S>& window, eClippingWindowEdges edge)
{
//Assume that we're clipping a polgon's edge with vertices at (x1,y1) and (x2,y2) against a clip window with vertices at (xmin, ymin) and (xmax,ymax).
//
//The location (IX, IY) of the intersection of the edge with the left side of the window is:
//
//   1. IX = xmin
//   2. IY = slope*(xmin-x1) + y1, where the slope = (y2-y1)/(x2-x1) 
//
//The location of the intersection of the edge with the right side of the window is:
//
//   1. IX = xmax
//   2. IY = slope*(xmax-x1) + y1, where the slope = (y2-y1)/(x2-x1) 
//
//The intersection of the polygon's edge with the top side of the window is:
//
//   1. IX = x1 + (ymax - y1) / slope
//   2. IY = ymax 
//
//Finally, the intersection of the edge with the bottom side of the window is:
//
//   1. IX = x1 + (ymin - y1) / slope
//   2. IY = ymin 
  const T slope = (to.y - from.y)/(to.x - from.x);
  switch (edge)
  {
  case eEdgeLeft:
    return Point2D<T>((T)window.low.x, (slope * ((T)window.low.x - from.x)) + from.y);
  case eEdgeRight:
    return Point2D<T>((T)window.high.x, (slope * ((T)window.high.x - from.x)) + from.y);
  case eEdgeTop:
    return Point2D<T>(from.x + (((T)window.high.y - from.y) / slope), (T)window.high.y);
  case eEdgeBottom:
    return Point2D<T>(from.x + (((T)window.low.y - from.y) / slope), (T)window.low.y);
  default:
    _ML_ASSERT(false);
    return Point2D<T>(0,0);
  }
}

template <typename T, typename S>
inline std::vector< Point2D<T> >
ClipEdge2D(const std::vector< Point2D<T> >& in, const Box2D<S>& window, eClippingWindowEdges edge)
{
  std::vector< Point2D<T> > out;
  if (in.size() <= 0)
    return out;
  Point2D<T> s = in[in.size()-1];
  for (unsigned int j = 0; j < in.size(); j++)
  {
    const Point2D<T> p = in[j];
    if (ClipPointInside(p, window, edge)) // {Cases 1 and 4}
    {
      if (ClipPointInside(s, window, edge)) // {Case 1}
      {
        out.push_back(p);
      }
      else
      {
        Point2D<T> i = ClipPointIntersect(s, p, window, edge);
        out.push_back(i);
        out.push_back(p);
      }
    }
    else // {Cases 2 and 3}
    {
      if (ClipPointInside(s, window, edge))
      {
        Point2D<T> i = ClipPointIntersect(s, p, window, edge);
        out.push_back(i);
      }
    }
    s = p;
  }
  return out;
}

template <typename T, typename S>
inline std::vector< Point2D<T> >
ClipPolygon2D(const std::vector< Point2D<T> >& p, const Box2D<S>& window)
{
  return ClipEdge2D(ClipEdge2D(ClipEdge2D(ClipEdge2D(p, window, eEdgeTop), window, eEdgeLeft), window, eEdgeRight), window, eEdgeBottom);
}

template <typename T, typename S>
inline std::vector< Point2D<T> >
ClipPolygon2D(const Point2D<T>* p, unsigned int numPoints, const Box2D<S>& window)
{
  const std::vector< Point2D<T> > pv(p, p+numPoints);
  return ClipPolygon2D(pv, window);
}
// template instantiation
template std::vector< Point2D<float> > ClipPolygon2D(const Point2D<float>* p, unsigned int numPoints, const Box2D<float>& window);
template std::vector< Point2D<double> > ClipPolygon2D(const Point2D<double>* p, unsigned int numPoints, const Box2D<double>& window);
template std::vector< Point2D<float> > ClipPolygon2D(const Point2D<float>* p, unsigned int numPoints, const Box2D<double>& window);
template std::vector< Point2D<double> > ClipPolygon2D(const Point2D<double>* p, unsigned int numPoints, const Box2D<float>& window);
