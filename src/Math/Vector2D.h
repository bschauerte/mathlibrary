/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

/** @file Vector2D.h
 *
 * This file contains the following classes ...
 * ... as a base class ...
 * - Vec2D<T>
 * ... and it's extensions
 * - Point2D<T>
 * - Vector2D<T>
 *
 * Additional type definitions
 *   Vector2D<float>   = Vector2Df
 *   Vector2D<double>  = Vector2Dd
 *   Point2D<float>    = Point2Df;
 *   Point2D<double>   = Point2Dd;
 *
 * See additional wrappers at the end of the file, e.g. to support OpenCV.
 * These wrappers can be enabled by defining the corresponding macros.
 *   _ENABLE_VECTOR2D_OPENCV_WRAPPER
 *     // @note: it may be necessary to place
 *     //   #define _ENABLE_VECTOR2D_OPENCV_WRAPPER
 *     //   #include "Math/Vector2D.h"
 *     // BEFORE including "opencv/cv.h"
 *
 * @author Boris Schauerte
 * @date 2009
 */
#pragma once

#include "_ml_assert.h"

#include "Math/Common.h"

#include <math.h>

#include <iostream>
#include <string>

/* The classes */
template <typename T> struct Vec2D;
template <typename T> struct Point2D;
template <typename T> struct Vector2D;
template <typename T> struct Box2D;

/* Additional type definitions */
typedef Vector2D<float> Vector2Df;
typedef Vector2D<double> Vector2Dd;
typedef Point2D<float> Point2Df;
typedef Point2D<double> Point2Dd;
typedef Box2D<float> Box2Df;
typedef Box2D<double> Box2Dd;

/**
 * @class Vec2D
 *
 * Base class for Vector2D and Point2D.
 * 3D base representation to support working with 3x3 transformation matrices.
 *
 * @author Boris Schauerte
 * @date 2009
 */
template <typename T>
struct Vec2D
{
public:
  /** Assignment operator */
  inline void operator=(const Vec2D<T>& source)
  {
    x = (T)source.x;
    y = (T)source.y;
    c = (T)source.c;
  }
  /** Return the angle of the vector. */
  inline T Angle(void) const
  {
    return (T)atan2((T)y,(T)x);
  }
  /** Absolute value (square of the length) of the vector. */
  inline T Abs(void) const
  {
    return SQR(x) + SQR(y);
  }
  /** Length of the vector. */
  inline T Length(void) const
  {
    return SQRT(SQR(x) + SQR(y));
  }
  /** Dot-product. */
  inline T DotProduct(const Vec2D<T>& v) const
  {
    return (x*v.x) + (y*v.y);
  }
  /** Normalize the vector to length 1. */
  inline void Normalize(void)
  {
    T l = Length();
    x /= l;
    y /= l;
  }

  T x,y;
  T c;
protected:
  Vec2D(void)
    : x(0), y(0), c(0)
  {
  }
  Vec2D(T _x, T _y, T _c)
    : x(_x), y(_y), c(_c)
  {
  }
  ~Vec2D(void)
  {
  }
private:
};

template <typename T>
inline T
DotProduct(const Vec2D<T>& u, const Vec2D<T>& v)
{
  return (u.x*v.x) + (u.y*v.y);
}

template <typename T> std::ostream& operator<<(std::ostream &os, const Vec2D<T>& v);
template <typename T> std::istream& operator>>(std::istream &is, Vec2D<T>& v);

/**
 * @class Vector2D
 *
 * Representation of a vector in 2D-space.
 *
 * @author Boris Schauerte
 * @date 2009
 */
template <typename T>
struct Vector2D
  : public Vec2D<T>
{
public:
  Vector2D(void)
    : Vec2D<T>()
  {
  }
  Vector2D(T _x)
    : Vec2D<T>(_x,_x, (T)1)
  {
  }
  Vector2D(T _x, T _y)
    : Vec2D<T>(_x, _y, (T)1)
  {
  }
  ~Vector2D(void)
  {
  }


  /** Scale */
  inline void operator*=(T factor)
  {
    Vec2D<T>::x *= factor;
    Vec2D<T>::y *= factor;
  }
  /** Divide */
  inline void operator/=(T divisor)
  {
    _ML_ASSERT(divisor != 0);

    Vec2D<T>::x /= divisor;
    Vec2D<T>::y /= divisor;
  }

  Point2D<T> operator-(const Point2D<T>& other) const
  {
    return Point2D<T>(this->x - other.x, this->y - other.y);
  }
  Point2D<T> operator+(const Point2D<T>& other) const
  {
    return Point2D<T>(this->x + other.x, this->y + other.y);
  }

  Vector2D<T> operator+(const Vector2D<T>& other) const
  {
    return Vector2D<T>(this->x + other.x, this->y + other.y);
  }
  Vector2D<T> operator-(const Vector2D<T>& other) const
  {
    return Vector2D<T>(this->x - other.x, this->y - other.y);
  }
protected:
private:
};

template <typename T>
inline Vector2D<T> operator*(const Vector2D<T>& v, const T s)
{
  Vector2D<T> _v(v);
  _v *= s;
  return _v;
}

template <typename T>
inline Vector2D<T> operator*(const T s, const Vector2D<T>& v)
{
  Vector2D<T> _v(v);
  _v *= s;
  return _v;
}

template <typename T>
inline Vector2D<T> operator/(const Vector2D<T>& v, const T s)
{
  Vector2D<T> _v(v);
  _v /= s;
  return _v;
}

template <typename T>
inline Vector2D<T> operator/(const T s, const Vector2D<T>& v)
{
  Vector2D<T> _v(s/v.x, s/v.y);
  return _v;
}

/**
 * @class Point2D
 *
 * Representation of a point in 2D-space.
 *
 * @author Boris Schauerte
 * @date 2009
 */
template <typename T>
struct Point2D
  : public Vec2D<T>
{
public:
  Point2D(void)
    : Vec2D<T>(0, 0, 0)
  {
  }
  Point2D(T _x)
    : Vec2D<T>(_x,_x, (T)1)
  {
  }
  Point2D(T _x, T _y)
    : Vec2D<T>(_x, _y, (T)0)
  {
  }
  ~Point2D(void)
  {
  }

  Point2D<T> operator+(const Vector2D<T>& other) const
  {
    return Point2D<T>(this->x + other.x, this->y + other.y);
  }
  Point2D<T> operator-(const Vector2D<T>& other) const
  {
    return Point2D<T>(this->x - other.x, this->y - other.y);
  }
  Vector2D<T> operator-(const Point2D<T>& other) const
  {
    return Vector2D<T>(this->x - other.x, this->y - other.y);
  }
  Vector2D<T> operator+(const Point2D<T>& other) const
  {
    return Vector2D<T>(this->x + other.x, this->y + other.y);
  }
  Point2D<T> operator-() const
  {
    return Point2D<T>(-this->x, -this->y);
  }
protected:
private:
};

/**
 * Box2D
 *
 * Helper data structure to describe a 2-dimensional box.
 * For correct functionality it is necessary that high.x >= low.x && high.y >= low.y.
 *
 * @author Boris Schauerte
 * @date 2009
 */
template <typename T>
struct Box2D
{
public:
  Box2D(void)
  {
  }
  Box2D(const Point2D<T>& p1, const Point2D<T>& p2)
  : low(p1), high(p2)
  {
  }

  /** Area of the 2D box. */
  inline T Area(void) const { return (high.x - low.x)*(high.y - low.y); }

  Point2D<T> low;
  Point2D<T> high;
protected:
private:
};

/* ADDITIONAL FUNCTIONALITY */
#ifdef _ENABLE_VECTOR2D_OPENCV_WRAPPER
#ifndef _CV_H_
#include "opencv/cv.h" // make sure the header -- and thus the OpenCV types -- are available
#endif

template <typename T>
inline
CvPoint cvPoint(const Vec2D<T>& v)
{
  return cvPoint((int)v.x,(int)v.y);
}
template <typename T>
inline
CvPoint2D32f cvPoint2D32f(const Vec2D<T>& v)
{
  return cvPoint2D32f(v.x,v.y);
}
template <typename T>
inline
CvRect cvRect(const Box2D<T>& b)
{
  return cvRect(b.low.x, b.low.y, b.high.x - b.low.x, b.high.y - b.low.y);
}
template <typename T>
inline
CvRect cvRect(const Point2D<T>& p, const Vector2D<T>& v)
{
  return cvRect(p.x, p.y, v.x, v.y);
}
#endif
