/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#include "_ml_base_def.h"
#include "GaussianWeight.h"

#include <stddef.h>

template<typename T>
GaussianWeight<T>::GaussianWeight(const T _stddev, const T _step)
: stddev(_stddev), step(_step)
{
  // automatische Wahl der Anzahl von Samplingschrittne es gilt ungef�hr ...
  // 68,27 % aller Messwerte haben eine Abweichung von h�chstens stddev vom Mittelwert
  // 95,45 % aller Messwerte haben eine Abweichung von h�chstens 2stddev vom Mittelwert
  // 99,73 % aller Messwerte haben eine Abweichung von h�chstens 3stddev vom Mittelwert.
  numSteps = (unsigned int)ceil(2*stddev / step);

  weights = new T[numSteps+1];
  T variance = 2*SQR(stddev);
  T current = 0;
  for (unsigned int i = 0; i < numSteps+1; i++)
  {
    weights[i] = exp(-SQR(current) / variance);
    current += step;
  }
}

template<typename T>
GaussianWeight<T>::GaussianWeight(const T _stddev, const T _step, const unsigned int _numSteps)
: stddev(_stddev), step(_step), numSteps(_numSteps)
{
  weights = new T[numSteps+1];
  T variance = 2*SQR(stddev);
  T current = 0;
  for (unsigned int i = 0; i < numSteps+1; i++)
  {
    weights[i] = exp(-SQR(current) / variance);
    current += step;
  }
}

template<typename T>
GaussianWeight<T>::~GaussianWeight(void)
{
  if (weights != NULL)
    delete [] weights;
}

// template instantiation
template class GaussianWeight<float>;
template class GaussianWeight<double>;
