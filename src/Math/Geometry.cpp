/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#include "_ml_base_def.h"
#include "Geometry.h"

#include <float.h> // FLT_MAX

/*
 * The theta function returns a value, that has the same ordering
 * effect as the arcus-tangens, but the result does not have to
 * be an angle.
 * See: R. Sedgewick - Algorithms in C
 */
template <typename T>
inline T
Theta(const Point2D<T>& p1, const Point2D<T>& p2)
{
  T dx(p2.x - p1.x);
  T dy(p2.y - p1.y);

#ifdef _THETA_USE_ATAN2 // use the standard atan2-function (returns an angle)
  // untested
  double result = atan2(dy,dx);
  if (result < 0)
    result = -1 * result + pi;
  return result;
#else // don't use atan (does not necessarily return an angle)
  T ax = fabs(dx), ay = fabs(dy);
  T t;

  t = (ax+ay == 0) ? 0 : dy/(ax+ay);
  if (dx < 0)
    t = 2-t;
  else
    if(dy < 0)
      t = 4+t;
  return t*(T)90.0;
#endif
}

/*
 * Swap. Avoid naming collision with stl.
 */
template <typename T>
inline void
Exch(Point2D<T> *ptr, int a, int b) {
  Point2D<T> tmp(ptr[a]);
  ptr[a] = ptr[b];
  ptr[b] = tmp;
}

template <typename T, typename S>
S
CalculateConvexHull(Point2D<T> p[], Point2D<T> hull[], const S N)
{
  /* find lowest (min.y) rightmost point */
  S min = 0;
  for (S i = 1; i < N; i++)
    if (p[i].y < p[min].y)
      min = i;
  for (S i = 0; i < N; i++)
    if (p[i].y == p[min].y)
      if(p[i].x > p[min].x)
        min = i;
  Exch<T>(p, min, 0);  // reference point at position 0 in the array

  /* Shellsort with sequence: 2^k - 1  =>  Complexity: O(n^1.5) */
  int start = 1;  // p[0] is the reference point and does not need to be sorted
  int end = N-1;
  int h;
  // shellsort -> find first sorting shell
  for(h = 1; h < (end - start); h = 3*h + 1);
  for (; h > 0; h /= 3) {
    int i;
    for (i = 0; i <= end; i += h) {
      int j;
      for (j = i; j >= start; j -= h) {
        if (j > 0)
        {
          // if theta of p[j] is samller than theta of p[j-h] ...
          if (Theta<T>(p[0], p[j]) < Theta<T>(p[0], p[j-h]) ||
          // ... or equal and p[j] is farther away from p[0] than p[j-h] ...
             (Theta<T>(p[0], p[j]) == Theta<T>(p[0], p[j-h]) && (p[0] - p[j]).Abs() > (p[0] - p[j-h]).Abs()))
            // ... then swap p[j] and p[j-h]
            Exch<T>(p, j, j - h);
        }
      }
    }
  }

  // Simulate a stack with the output array
  int stackCount = -1;  // Top of stack is always at pos. stackCount
#define __PUSH_STACK(x) stackCount++;\
                        hull[stackCount] = x;
#define __POP_STACK()   stackCount--;
  // Note that p[0] and p[1] belong in any case to the convex hull
  // (effect of the sorting by angle)
  __PUSH_STACK(p[0]);
  __PUSH_STACK(p[1]);

  S i = 2;
  while (i < N) // check every point
  {
    Point2D<T> pt1(hull[stackCount]);     // pt1 : top of stack
    Point2D<T> pt2(hull[stackCount-1]);   // pt2 : second top of stack
    if (IsLeft(pt2, pt1, p[i]) > 0)
    {
      __PUSH_STACK(p[i]);
      i++;
    }
    else  // isLeft(pt2, pt1, p[i]) <= 0
          //   <0    => point is right, pt1 does not belong to convex hull
          //   == 0  => points are collinear, choose the point (p[i] or pt1) that is
          //            farther away from pt2
          // Note:
          //  if you want all points that lie on the convex hull, than you
          //  can remove the '='-case
      if (IsLeft(pt2, pt1, p[i]) < 0)
      {
        __POP_STACK();
      }
      else  // isLeft == 0
      {
        if((pt2 - p[i]).Abs() >= (pt2 - pt1).Abs())
        {
          __POP_STACK();
          __PUSH_STACK(p[i]);
          i++;
        }
        else
        {
          i++;
        }
      }
  }
  return stackCount+1;  // number of points of the convex hull
#undef __PUSH_STACK
#undef __POP_STACK
}
// template instantiation
template int CalculateConvexHull(Point2D<float> p[], Point2D<float> hull[], const int N);
template unsigned int CalculateConvexHull(Point2D<float> p[], Point2D<float> hull[], const unsigned int N);
template int CalculateConvexHull(Point2D<double> p[], Point2D<double> hull[], const int N);
template unsigned int CalculateConvexHull(Point2D<double> p[], Point2D<double> hull[], const unsigned int N);

template <typename T>
bool
IntersectsRayBox(const Point3D<T>& o, const Vector3D<T>& d, // origin and direction of the ray
                 const Box3D<T>& box)                       // the box to check against
{
  /* Kay and Kayjia "slabs" algorithms for Ray-Box-Intersection test. */
  T tNear = -FLT_MAX;
  T tFar = +FLT_MAX;

  // simple checks (and avoid div. by zero)
  if (d.x == 0 && (o.x < box.low.x || o.x > box.high.x))
    return false;
  if (d.y == 0 && (o.y < box.low.y || o.y > box.high.y))
    return false;
  if (d.z == 0 && (o.z < box.low.z || o.z > box.high.z))
    return false;

  // x direction
  {
    T t1 = (box.low.x - o.x) / d.x;                            // compute intersection distance to the (box-)planes
    T t2 = (box.high.x - o.x) / d.x;                           // compute intersection distance to the (box-)planes
    if (t1 > t2) { const T tmp = t1; t1 = t2; t2 = tmp; }      // swap, ensure t1 <= t2
    if (t1 > tNear) tNear = t1;                                // want largest Tnear
    if (t2 < tFar) tFar = t2;                                  // want smallest Tfar
    if (tNear > tFar) return false;                            // box is missed
    if (tFar < 0) return false;                                // box is behind ray
  }
  // y direction
  {
    T t1 = (box.low.y - o.y) / d.y;
    T t2 = (box.high.y - o.y) / d.y;
    if (t1 > t2) { const T tmp = t1; t1 = t2; t2 = tmp; } // swap
    if (t1 > tNear) tNear = t1;
    if (t2 < tFar) tFar = t2;
    if (tNear > tFar) return false; // box is missed
    if (tFar < 0) return false; // box is behind ray
  }
  // z direction
  {
    T t1 = (box.low.z - o.z) / d.z;
    T t2 = (box.high.z - o.z) / d.z;
    if (t1 > t2) { const T tmp = t1; t1 = t2; t2 = tmp; } // swap
    if (t1 > tNear) tNear = t1;
    if (t2 < tFar) tFar = t2;
    if (tNear > tFar) return false; // box is missed
    if (tFar < 0) return false; // box is behind ray
  }
  return true;
}
// template instantiation
template bool IntersectsRayBox(const Point3D<float>& o, const Vector3D<float>& d, const Box3D<float>& box);
template bool IntersectsRayBox(const Point3D<double>& o, const Vector3D<double>& d, const Box3D<double>& box);

template <typename T, typename S>
T
PolygonArea(const Point2D<T>* points, const S numPoints, bool unsignedArea)
{
  if (numPoints < 3) // at least 3 points necessary to form a valid polygon
  {
    _ML_ASSERT(false);
    return 0;
  }

  // calculate area: http://de.wikipedia.org/wiki/Gau%C3%9Fsche_Trapezformel
  T area = 0;
  for (unsigned int i = 0; i < ((unsigned int)numPoints) - 1; i++)
    area += (points[i].x + points[i+1].x) * (points[i+1].y - points[i].y);
  area += (points[numPoints-1].x + points[0].x) * (points[0].y - points[numPoints-1].y);

  if (area < 0 && unsignedArea == true) // sign depends on polygon order (CW,CCW)
    return (-area / 2);
  else
    return (area / 2);
}
// template instantiation
template float PolygonArea(const Point2D<float>* points, const unsigned int numPoints, bool unsignedArea);
template float PolygonArea(const Point2D<float>* points, const int numPoints, bool unsignedArea);
template double PolygonArea(const Point2D<double>* points, const unsigned int numPoints, bool unsignedArea);
template double PolygonArea(const Point2D<double>* points, const int numPoints, bool unsignedArea);


/* Calculate the centroid of a simple polygon */
template <typename T, typename S>
Point2D<T>
PolygonCentroid(const Point2D<T>* points, const S numPoints)
{
  if (numPoints < 3) // at least 3 points necessary to form a valid polygon
  {
    _ML_ASSERT(false);
    return 0;
  }

  // calculate area: http://de.wikipedia.org/wiki/Gau%C3%9Fsche_Trapezformel
  const T area = PolygonArea(points, numPoints, false);
  Point2D<T> centroid(0,0);
  for (unsigned int i = 0; i < numPoints-1; i++)
  {
    centroid.x += (points[i].x + points[i+1].x) * (points[i+1].y*points[i].x - points[i+1].x*points[i].y);
    centroid.y += (points[i].y + points[i+1].y) * (points[i+1].y*points[i].x - points[i+1].x*points[i].y);
  }
  centroid.x += (points[numPoints-1].x + points[0].x) * (points[0].y*points[numPoints-1].x - points[0].x*points[numPoints-1].y);
  centroid.y += (points[numPoints-1].y + points[0].y) * (points[0].y*points[numPoints-1].x - points[0].x*points[numPoints-1].y);

  centroid /= (6*area);
}
