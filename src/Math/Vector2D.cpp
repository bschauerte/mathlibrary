/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#include "_ml_base_def.h"
#include "Vector2D.h"

template <typename T>
std::istream& operator>>(std::istream &is, Vec2D<T>& v)
{
  char c;

#define __STREAM_READIN(prefix,var) is >> c;\
    if (c == prefix)\
      is >> var;\
    else\
    {\
      is.clear(std::ios::badbit);\
      return is;\
    }

  __STREAM_READIN('(',v.x);
  __STREAM_READIN(',',v.y);
  __STREAM_READIN(',',v.c);

  is >> c;
  if (c != ')')
    is.clear(std::ios::badbit);

#undef __STREAM_READIN

  return is;
}

template <typename T>
std::ostream&
operator<<(std::ostream &os, const Vec2D<T>& v)
{
  os << "(" << v.x << "," << v.y << "," << v.c << ")";
  return os;
}

template std::ostream& operator<<(std::ostream &os, const Vec2D<float>& v);
template std::ostream& operator<<(std::ostream &os, const Vec2D<double>& v);
template std::istream& operator>>(std::istream &is, Vec2D<float>& v);
template std::istream& operator>>(std::istream &is, Vec2D<double>& v);
