/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#include "_ml_base_def.h"
#include "VectorND.h"

#include "Math/Common.h"

#include <cmath>
#include <string.h>

/************ VectorND ************/
template<typename T>
VectorND<T>::VectorND(int dim, T initval)
{
	elements = new T[dim];
	for (int i=0; i<dim; i++)
	  elements[i] = initval;
	m_dim = dim;
	m_init = true;
}

template<typename T>
VectorND<T>::VectorND(void)
{
	elements = 0;
	m_dim = -1;
	m_init = false;
}

template<typename T>
void
VectorND<T>::Reset(void)
{
	memset(elements, 0, m_dim * sizeof(T));
}

template<typename T>
void
VectorND<T>::Reset(T val)
{
	for (int i=0; i<m_dim; i++)
	  elements[i] = val;
}

template<typename T>
void
VectorND<T>::Init(int dim, T initval)
{
	elements = new T[dim];
	for (int i=0; i<dim; i++)
	  elements[i] = initval;
	m_dim = dim;
	m_init = true;
}

template<typename T>
VectorND<T>::VectorND(const VectorND<T>& src)
{
	elements = new T[src.Dim()];
	m_dim = src.Dim();
	for (int i=0; i<m_dim; i++)
	  elements[i] = src[i];
	m_init = true;
}

template<typename T>
VectorND<T>::~VectorND(void)
{
	delete [] elements;
}

template<typename T>
VectorND<T>::VectorND(const Vec3D<T>& v)
{
	elements = new T[3];
	m_dim = 3;
	elements[0] = v.x;
	elements[1] = v.y;
	elements[2] = v.z;
	m_init = true;
}

template<typename T>
void
VectorND<T>::Set(Vec3D<T> v)
{
	if (m_dim == 3)
	{
		elements[0] = v.x;
		elements[1] = v.y;
		elements[2] = v.z;
	}
	else
	  std::cerr << "<VECTORND>::Set: ERROR: Vector sizes do not match." << std::endl;
}

template<typename T>
void
VectorND<T>::Set(T* src)
{
  	memcpy(elements, src, m_dim * sizeof(T));
}

template<typename T>
void VectorND<T>::Set(const VectorND<T> v, int idx, int range)
{
	if (idx+range-1 < m_dim)
	{
		for (int j=0; j<range; j++)
			elements[idx+j] = v[j];
	}
	else
	  std::cerr << "<VECTORND>::Set(range): WARNING: Index out of bounds!" << std::endl;
}

template<typename T>
T&
VectorND<T>::Get(int i)
{
  if (i < m_dim)
    return elements[i];

	std::cerr << "<VECTORND>::Get: WARNING: Index out of bounds!" << std::endl;
	return elements[0];
}

template<typename T>
VectorND<T>
VectorND<T>::Get(int i, int range)
{
	VectorND<T> t(range);

  if (i+range-1 < m_dim)
	{
		for (int j=0; j<range; j++)
			t[j] = elements[i+j];
		return t;
	}
	std::cerr << "<VECTORND>::Get(range): WARNING: Index out of bounds!" << std::endl;
	return t;
}

/*
template<typename T>
void
VectorND<T>::Set(VectorND<T> src)
{
	if (m_dim == src.dim())
	{
  		for (int i=0; i<m_dim; i++)	elements[i] = src[i];
	}
	else
		fprintf(stderr, "<VECTORND>: ERROR: Vector sizes do not match.\n");
}*/

template<typename T>
double
VectorND<T>::Length(void) const
{
  double ret = 0;
	for (int i=0; i<m_dim; i++)
	  ret += SQR(elements[i]);
	return SQRT(ret);
}

template<typename T>
double
VectorND<T>::Length(int idx, int range) const
{
	if (idx+range-1 < m_dim)
	{
  	double ret = 0.0;
		for (int i=0; i<range; i++)
		  ret += SQR(elements[idx+i]);
		return SQRT(ret);
	}
	std::cerr << "<VECTORND>::Length: WARNING: Index out of bounds! " << idx << " " << range << " " << m_dim << std::endl;
	return 0;
}


template<typename T>
void
VectorND<T>::Normalize(void)
{
  double l = Length();
  for (int i=0; i<m_dim; i++)
    elements[i] = (T)((double)elements[i] / l);
}

template<typename T>
T&
VectorND<T>::operator[](int i) const
{
  if (i < m_dim)
    return elements[i];

	std::cerr << "<VECTORND>::operator[]: WARNING: Index out of bounds! " << i << " " << m_dim << std::endl;
	return elements[0];
}

template<typename T>
VectorND<T>&
VectorND<T>::operator*=(T s)
{
  for (int i=0; i<m_dim; i++)
    elements[i] *= s;
  return *this;
}

template<typename T>
VectorND<T>&
VectorND<T>::operator/=(T s)
{
  for (int i=0; i<m_dim; i++)
    elements[i] /= s;
  return *this;
}

template<typename T>
VectorND<T>&
VectorND<T>::operator-(void)
{
  for (int i=0; i<m_dim; i++)
    elements[i] = -elements[i];
  return *this;
}

template<typename T>
void
VectorND<T>::operator=(const VectorND<T>& source)
{
   for (int i=0; i<m_dim; i++)
     elements[i] = source[i];
}

template<typename T>
T
VectorND<T>::DotProduct(const VectorND<T>& v)
{
   T ret = 0;
   for (int i=0; i<m_dim; i++)
     ret += v[i]*elements[i];
   return ret;
}

template<typename T>
void
VectorND<T>::ElemMul(const VectorND<T>& source)
{
   for (int i=0; i<m_dim; i++)
     elements[i] *= source[i];
}

template<typename T>
void
VectorND<T>::ElemDiv(const VectorND<T>& source)
{
   for (int i=0; i<m_dim; i++)
     elements[i] /= source[i];
}

template<typename T>
T
VectorND<T>::ElemMax(void)
{
   T ret = elements[0];
   for (int i=1; i<m_dim; i++)
     ret = (elements[i] > ret ? elements[i] : ret);
   return ret;
}

template<typename T>
T
VectorND<T>::ElemMin(void)
{
   T ret = elements[0];
   for (int i=1; i<m_dim; i++)
     ret = (elements[i] < ret ? elements[i] : ret);
   return ret;
}

template<typename T>
void
VectorND<T>::ElemAbs(void)
{
  for (int i=0; i<m_dim; i++)
    if (elements[i] < 0)
      elements[i] = -elements[i];
}

template<typename T>
void
VectorND<T>::ElemSign(void)
{
  for (int i=0; i<m_dim; i++)
  {
  	if (elements[i] < 0)
  	  elements[i] = (T)-1;
  	else
  	  if (elements[i] > 0)
  	    elements[i] = (T)1;
  }
}

template<typename T>
bool
VectorND<T>::Equal(const VectorND<T>& v, T tolerance)
{
   /*for (int i=1; i<m_dim; i++)
   {
	if (((elements[i]-v[i]) < -tolerance) || ((elements[i]-v[i]) > tolerance))	return false;
   }*/

  if ((*this - v).Length() > tolerance)
    return false;
  else
   return true;
}

template<typename T>
bool
VectorND<T>::Equal(const VectorND<T>& v, int idx, int range, T tolerance)
{
   /*for (int i=1; i<m_dim; i++)
   {
	if (((elements[i]-v[i]) < -tolerance) || ((elements[i]-v[i]) > tolerance))	return false;
   }*/
  VectorND<T> t(range);
  for (int i=0; i<range; i++)
    t[i] = elements[i+idx] - v[i];
  if (t.Length() > tolerance)
    return false;

  return true;
}

/************ OPERATORS ************/
template <typename T>
VectorND<T> operator*(const VectorND<T>& v, T s)
{
  VectorND<T> _v(v);
  _v *= s;
  return _v;
}

template <typename T>
VectorND<T> operator*(T s, const VectorND<T>& v)
{
  VectorND<T> _v(v);
  _v *= s;
  return _v;
}

template <typename T>
VectorND<T> operator*(const VectorND<T>& v, unsigned int i)
{
  return (v * (T)i);
}

template <typename T>
VectorND<T> operator*(unsigned int i, const VectorND<T>& v)
{
  return (v * (T)i);
}

template <typename T>
VectorND<T> operator/(const VectorND<T>& v, T s)
{
  VectorND<T> _v(v);
  _v /= s;
  return _v;
}

template <typename T>
VectorND<T> operator/(T s, const VectorND<T>& v)
{
  VectorND<T> _v(v);
  _v /= s;
  return _v;
}

template <typename T>
VectorND<T> operator/(const VectorND<T>& v, unsigned int i)
{
  return (v / (T)i);
}

template <typename T>
VectorND<T> operator/(unsigned int i, const VectorND<T>& v)
{
  return (v / (T)i);
}


template <typename T>
VectorND<T> operator+(const VectorND<T>& v1, const VectorND<T>& v2)
{
  VectorND<T> _v(v1);
  for (int i=0; i<_v.Dim(); i++)
    _v[i] += v2[i];
  return _v;
}

template <typename T>
VectorND<T> operator-(const VectorND<T>& v1, const VectorND<T>& v2)
{
  VectorND<T> _v(v1);
  for (int i=0; i<_v.Dim(); i++)
    _v[i] -= v2[i];
  return _v;
}

template class VectorND<double>;
template class VectorND<float>;
template class VectorND<int>;

__INSTANTIATE_VND_OPERATORS(double)
__INSTANTIATE_VND_OPERATORS(float)
__INSTANTIATE_VND_OPERATORS(int)
