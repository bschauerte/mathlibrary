/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

/**
 * @class PreciseSum
 *
 * Precise iterative (error compensated) summation - includes estimated error during the calculation to improve the accuracy.
 * Inspired by / Taken from the esmeralda sum.c code ...
 *
 * @author Boris Schauerte
 * @date 2009
 */
template <typename T>
class PreciseSum
{
public:
  PreciseSum(void)
  : sum(0), error(0)
  {
  }
  ~PreciseSum(void)
  {
  }
  /** Add an element to the sum */
  inline const T& Add(const T& x)
  {
    T y, z;

    /* perform (error) compensated summation ... */
    y = x - error;
    z = sum + y;
    error = (z - sum) - y;
    sum = z;

    return sum;
  }
  inline void Reset(void) { sum = T(0); error = T(0); }
  inline const PreciseSum& operator+=(const T& x) { Add(x); return *this; }
  /** Get the calculated sum */
  inline const T& GetSum(void) const { return sum; }
  /** Get the estimated error */
  inline const T& GetError(void) const { return error; }
protected:
private:
  T sum;
  T error;
};
