/**
 * Copyright 2009 J. Richarz. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

/** @file VectorND.h
 *
 *  This file contains the following classes ...
 * - VectorND<T>
 *
 * Additional type definitions
 *   VectorND<float>   = VectorNDf
 *   VectorND<double>  = VectorNDd
 *
 * @author Jan Richarz
 * @date 2009
 */
#pragma once

#include "Math/Vector3D.h"

/**
 * @class VectorND
 *
 * Vector representation of a n-dimensional point
 *
 * @author Jan Richarz
 * @date 2009
 */
template <typename T>
class VectorND
{
public:
  VectorND(void);
  VectorND(int dimension, T initval = 0);
  VectorND(const VectorND<T>& src);
  ~VectorND(void);
  void Reset(void);
  void Reset(T val);
  void Init(int dimensions, T initval = 0);
  bool IsInit(void) const { return m_init; }
  void Set(T* src);
  void Set(const VectorND<T> v, int idx, int range);
 // void Set(const VectorND<T> src);

  T& Get(int idx);
  VectorND<T> Get(int idx, int range);

  VectorND(const Vec3D<T>& v);
  void Set(const Vec3D<T> v);

  // All the below operations are ND, i.e. the constant is not considered
  /* Length of the vector (uses euclidean metric) */
  double Length(void) const;
  double Length(int idx, int range) const;
  /* Normalize the vector (i.e. after normalization Length=1.0) */
  void Normalize(void);
  /* Provide array access */
  T& operator[](int i) const;
  /* Scaling */
  VectorND& operator*=(T s);
  VectorND& operator/=(T s);
  /* aka. *= -1.0 */
  VectorND& operator-(void);
  /* Assignment operator */
  void operator=(const VectorND<T>& source);

  /* Dot-product. */
  T DotProduct(const VectorND<T>& v);

   /* Element-wise multiplication. The constant c will be ignored. */
  void ElemMul(const VectorND<T>& source);

  /* Element-wise division. The constant c will be ignored. */
  void ElemDiv(const VectorND<T>& source);

  /* Calculate the max. of x, y and z. */
  T ElemMax(void);

  /* Calculate the min. of x, y and z. */
  T ElemMin(void);

  /* Element-wise absolute value operation. */
  void ElemAbs(void);

  /* Element-wise signum function */
  void ElemSign(void);

  /* query vector dimension */
  int Dim(void) const {return m_dim;};

  /* element-wise equality operation with tolerance value */
  bool Equal(const VectorND<T>& v, T tolerance = (T)0);

  /* element-wise equality operation with tolerance value over a range of elements*/
  bool Equal(const VectorND<T>& v, int idx, int range, T tolerance = (T)0);
protected:
  T* elements;
  int m_dim;
  bool m_init;
};

/* Scaling operators */
template <typename T> VectorND<T> operator*(const VectorND<T>& v, T s);
template <typename T> VectorND<T> operator*(T s, const VectorND<T>& v);
template <typename T> VectorND<T> operator*(const VectorND<T>& v, unsigned int i);
template <typename T> VectorND<T> operator*(unsigned int i, const VectorND<T>& v);
template <typename T> VectorND<T> operator/(const VectorND<T>& v, T s);
template <typename T> VectorND<T> operator/(T s, const VectorND<T>& v);
template <typename T> VectorND<T> operator/(const VectorND<T>& v, unsigned int i);
template <typename T> VectorND<T> operator/(unsigned int i, const VectorND<T>& v);

template <typename T> VectorND<T> operator+(const VectorND<T>& v1, const VectorND<T>& v2);
template <typename T> VectorND<T> operator-(const VectorND<T>& v1, const VectorND<T>& v2);

/* Additional type definitions */
typedef VectorND<float> VectorNDf;
typedef VectorND<double> VectorNDd;
typedef VectorND<int> VectorNDi;

#define __INSTANTIATE_VND_OPERATORS(_TYPENAME) \
  template VectorND<_TYPENAME> operator*(const VectorND<_TYPENAME>& v, _TYPENAME s); \
  template VectorND<_TYPENAME> operator*(_TYPENAME s, const VectorND<_TYPENAME>& v); \
  template VectorND<_TYPENAME> operator*(const VectorND<_TYPENAME>& v, unsigned int i); \
  template VectorND<_TYPENAME> operator*(unsigned int i, const VectorND<_TYPENAME>& v); \
  template VectorND<_TYPENAME> operator/(const VectorND<_TYPENAME>& v, _TYPENAME s); \
  template VectorND<_TYPENAME> operator/(_TYPENAME s, const VectorND<_TYPENAME>& v); \
  template VectorND<_TYPENAME> operator/(const VectorND<_TYPENAME>& v, unsigned int i); \
  template VectorND<_TYPENAME> operator/(unsigned int i, const VectorND<_TYPENAME>& v); \
  template VectorND<_TYPENAME> operator+(const VectorND<_TYPENAME>& v1, const VectorND<_TYPENAME>& v2); \
  template VectorND<_TYPENAME> operator-(const VectorND<_TYPENAME>& v1, const VectorND<_TYPENAME>& v2);
