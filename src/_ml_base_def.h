/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

/* _base_def
 *
 * _base_def has a similar function as stdafx.h.
 * - define required definitions
 * - add default includes
 * - specify compile-time options
 *(- ... but no precompiled header, not important for lib)
 * ... then why is it not named stdafx.h? To avoid naming problems under linux.
 *
 * @author Boris Schauerte
 * @date   2009
 */
#pragma once

/* Compiler and Library options */
// Windows specific options
#if defined _WIN32 || defined WIN32
#ifndef _WIN32_WINNT		// Lassen Sie die Verwendung spezifischer Features von Windows XP oder spter zu.
#define _WIN32_WINNT 0x0501	// ndern Sie dies in den geeigneten Wert fr andere Versionen von Windows.
#endif
#define WIN32_LEAN_AND_MEAN		// Selten verwendete Teile der Windows-Header nicht einbinden.
#endif
// VC options
#ifdef _MSC_VER
//#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES // Replace unsecure functions by the secure crt implementations (e.g. strcpy_s instead of strcpy)
// ... or ...
#define _CRT_SECURE_NO_WARNINGS // Just turn of the deprecation warnings of unsecure crt functions
#endif

#define WIN32_LEAN_AND_MEAN		// Selten verwendete Teile der Windows-Header nicht einbinden.

/*** Standard includes ***/
// ...

#ifndef DEBUG
// Turn boost asserts on/off
#define BOOST_DISABLE_ASSERTS
// Turn ML asserts on/off
#define DISABLE_ML_ASSERTS
#endif

