/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

#include "math.h"

#include "Math/Common.h"

/** @class GaussianWeight
 *
 * sample calculate gaussian weights Omega(x) for zero mean, given variance and given step size (for (sub-)sampling at step positions)
 * results in an array holding the weights
 *    [Omega(0), Omega(step), Omega(2*step), ..., Omega((numSteps)step)]
 * with omega
 *   exp{-(x)/(2*SQR(stddev))}
 * with stddev as standard deviation
 *
 * @author Boris Schauerte
 * @date 2009
 */
template <typename T>
class GaussianWeight
{
public:
  GaussianWeight(const T _stddev, const T _step);
  GaussianWeight(const T _stddev, const T _step, const unsigned int _numSteps);
  ~GaussianWeight(void);

  /** Get the used standard deviation. */
  inline T GetStdDev(void) const {return stddev; }
  /** Get the used step. */
  inline T GetStep(void) const {return step; }
  /** Number of steps. */
  inline unsigned int GetNumSteps(void) const { return numSteps; }

  /** Get the weight. */
  inline const T operator[](unsigned int idx) const
  {
    if (idx <= numSteps)
      return weights[idx];
    else
      return 0;
  }
  /** Enable array access to the weights. */
  inline const T operator[](int idx) const
  {
    if (idx < 0) // sysmmetrical, left/right side of the center have identical values
      idx *= -1;
    if (idx <= (int)numSteps)
      return weights[idx];
    else
      return 0;
  }
protected:
private:
  T stddev, step;
  unsigned int numSteps;
  T* weights;
};
