/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

#include "Math/Common.h"

#include "_ml_assert.h"

#include <cstring>

/*
Fuzzy-Interpretation
--------------------
- die Intensität/Salienz-Werte der SaliencySpaces sind in [0,1]
- interpretiere jeden SaliencySpace(-Voxel?) als Fuzzy-Menge (2-Klassen/LT "interessant"/"nicht-interessant").
- vereinige die Informationen aus den SaliencySpaces durch Anwendung von Fuzzy-Operatoren
  => anschließend DeFuzzyfizierung?
- auch möglich: die "Rohdaten" der SaliencySpaces umwandeln durch Zugehörigkeitsfunktion -> LT modellieren!!!
  => ohne Anwendung kann man das interpretieren als Zugehörigkeitsfunktion f(x)=ID(x)=x
  => Zugehörigkeitsfunktion definieren für "interessant"
*/

/**
 * @class FuzzySetOperators
 *
 * Basic fuzzy-set operators, to support implementation of an Fuzzy-interpretation of
 * SaliencySpace combination.
 *
 * @author Boris Schauerte
 * @date 2009
 */
class FuzzySetOperators
{
public:
  /* Supported dual triples (t-norm,s-norm,complement) */
  enum eSupportedTypes
  {
    eStandard,
    eLukasiewicz,
    eAlgebraic
  };

  /** Create a new FuzzySetOperators-Object by type */
  static FuzzySetOperators* Create(const eSupportedTypes type);
  /** Relase a FuzzySetOperators-Object */
  static void Release(FuzzySetOperators*& fso);

  // Interface for FuzzySetOperators implementions
  /** Virtual Complement method for FuzzySetOperators (interface). */
  virtual void Complement(const float* src, float* dst, const size_t size) const = 0;
  /** Virtual Union method for FuzzySetOperators (interface). */
  virtual void Union(const float* a, const float* b, float* c, const size_t size) const = 0;
  /** Virtual Intersection method for FuzzySetOperators (interface). */
  virtual void Intersection(const float* a, const float* b, float* c, const size_t size) const = 0;

  /* Fuzzy-Set Operators:
   * - complement                        b = c(a)
   * - union (t-norm)                    c = a v b
   * - intersection (s-norm, t-conorm)   c = a ^ b
   */
  // Standard operators/logic
  /** Standard Complement, i.e. c(a) = 1 - a. */
  inline static float StdComplement(const float a) { return 1.0f - a; }
  /** Standard Union, i.e. s(a,b) = max(a,b). */
  inline static float StdUnion(const float a, const float b) { return MAX(a, b); }
  /** Standard Intersection, i.e. t(a,b) = min(a,b) */
  inline static float StdIntersection(const float a, const float b) { return MIN(a, b); }
  /** Apply standard complement (i.e. c(a) = 1 - a) to array elements (i.e. fuzzy sets). */
  inline static void StdComplement(const float* src, float* dst, const size_t size)
  {
    for (size_t i = 0; i < size; i++)
      dst[i] = 1.0f - src[i];
  }
  /** Unite array elements (i.e. fuzzy sets) with the std. union (i.e. s(a,b) = max(a,b)). */
  inline static void StdUnion(const float* a, const float* b, float* c, const size_t size)
  {
    for (size_t i = 0; i < size; i++)
      c[i] = MAX(a[i], b[i]);
  }
  /** Intersect array elements (i.e. fuzzy sets) with the std. intersection (i.e. t(a,b) = min(a,b)). */
  inline static void StdIntersection(const float* a, const float* b, float* c, const size_t size)
  {
    for (size_t i = 0; i < size; i++)
      c[i] = MIN(a[i], b[i]);
  }
  // Lukasiewicz operators/logic
  /** Lukasiewicz/Standard Complement, i.e. c(a) = 1 - a. */
  inline static float LukaComplement(const float a) { return 1.0f - a; }
  /** Lukasiewicz Union, i.e. s(a,b) = min(1,a + b). */
  inline static float LukaUnion(const float a, const float b) { return MIN(1.0f, a + b); }
  /** Lukasiewicz Intersection, i.e. t(a,b) = max(0,a + b - 1). */
  inline static float LukaIntersection(const float a, const float b) { return MAX(0.0f, a + b - 1.0f); }
  /** Apply Lukasiewicz/standard complement (i.e. c(a) = 1 - a) to array elements (i.e. fuzzy sets). */
  inline static void LukaComplement(const float* src, float* dst, const size_t size)
  {
    for (size_t i = 0; i < size; i++)
      dst[i] = 1.0f - src[i];
  }
  /** Unite array elements (i.e. fuzzy sets) with the Lukasiewicz union (i.e. s(a,b) = min(1,a + b)). */
  inline static void LukaUnion(const float* a, const float* b, float* c, const size_t size)
  {
    for (size_t i = 0; i < size; i++)
      c[i] = MIN(1.0f, a[i] + b[i]);
  }
  /** Intersect array elements (i.e. fuzzy sets) with the Lukasiewicz intersection (i.e. t(a,b) = max(0,a + b - 1)). */
  inline static void LukaIntersection(const float* a, const float* b, float* c, const size_t size)
  {
    for (size_t i = 0; i < size; i++)
      c[i] = MAX(0.0f, a[i] + b[i] - 1.0f);
  }
  // algebraic operators/logic
  /** Algebraic/Standard Complement, i.e. c(a) = 1 - a. */
  inline static float AlgebraicComplement(const float a) { return 1.0f - a; }
  /** Algebraic Union, i.e. s(a,b) = a + b - (a*b). */
  inline static float AlgebraicUnion(const float a, const float b) { return a + b - (a*b); }
  /** Algebraic Intersection, i.e. t(a,b) = a*b. */
  inline static float AlgebraicIntersection(const float a, const float b) { return a*b; }
  /** Apply Algebraic/standard complement (i.e. c(a) = 1 - a) to array elements (i.e. fuzzy sets). */
  inline static void AlgebraicComplement(const float* src, float* dst, const size_t size)
  {
    for (size_t i = 0; i < size; i++)
      dst[i] = 1.0f - src[i];
  }
  /** Unite array elements (i.e. fuzzy sets) with the Algebraic union (i.e. s(a,b) = a + b - (a*b)). */
  inline static void AlgebraicUnion(const float* a, const float* b, float* c, const size_t size)
  {
    for (size_t i = 0; i < size; i++)
      c[i] = a[i] + b[i] - (a[i]*b[i]);
  }
  /** Intersect array elements (i.e. fuzzy sets) with the Algebraic intersection (i.e. t(a,b) = a*b). */
  inline static void AlgebraicIntersection(const float* a, const float* b, float* c, const size_t size)
  {
    for (size_t i = 0; i < size; i++)
      c[i] = a[i]*b[i];
  }
};

#define _MAKE_FSO(__name) class __name ## FSO : public FuzzySetOperators \
{\
public:\
  __name ## FSO (void) {}\
  void Complement(const float* src, float* dst, const size_t size) const { __name ## Complement(src,dst,size); }\
  void Union(const float* a, const float* b, float* c, const size_t size) const { __name ## Union(a,b,c,size); }\
  void Intersection(const float* a, const float* b, float* c, const size_t size) const { __name ## Intersection(a,b,c,size); }\
};

_MAKE_FSO(Std);
_MAKE_FSO(Luka);
_MAKE_FSO(Algebraic);

#undef _MAKE_FSO

/** Intersect a set of FuzzySets */
inline void
Intersect(float** spaces, size_t numSpaces, float* dst, unsigned int res, const FuzzySetOperators* fso)
{
  _ML_ASSERT(numSpaces > 0);

  memcpy((void*)dst, spaces[0], sizeof(float) * res); // initialize
  for (unsigned int i = 1; i < numSpaces; i++) // intersect
    fso->Intersection(spaces[i], dst, dst, res);
}

/** Unite a set of FuzzySets */
inline void
Unite(float** spaces, size_t numSpaces, float* dst, unsigned int res, const FuzzySetOperators* fso)
{
  _ML_ASSERT(numSpaces > 0);

  memcpy((void*)dst, spaces[0], sizeof(float) * res); // initialize
  for (unsigned int i = 1; i < numSpaces; i++) // intersect
    fso->Union(spaces[i], dst, dst, res);
}
