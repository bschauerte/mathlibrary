/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#include "_ml_base_def.h"
#include "MultiBaseNumber.h"

#include <string.h>
#include <iostream>

MultiBaseNumber::MultiBaseNumber(unsigned int _numDigits, unsigned int* _digitBases)
: digitBases(NULL), digits(NULL), numDigits(_numDigits)
{
  digitBases = new unsigned int[numDigits];
  memcpy((void*)digitBases, _digitBases, sizeof(unsigned int) * numDigits);

  digits = new unsigned int[numDigits];
  memset((void*)digits, 0, sizeof(unsigned int) * numDigits);

  numTotal = 1;
  for (unsigned int i = 0; i < numDigits; i++)
    numTotal *= digitBases[i];
}
MultiBaseNumber::~MultiBaseNumber(void)
{
  if (digitBases != NULL)
    delete [] digitBases;
  if (digits != NULL)
    delete [] digits;
}

void
MultiBaseNumber::cout(void) const
{
  std::cout << "[ ";
  for (unsigned int i = 0; i < numDigits; i++)
    std::cout << digits[i] << " ";
  std::cout << "]";
}

unsigned int
MultiBaseNumber::GetNumber(void) const
{
  unsigned int number = 0;
  unsigned int factor = 1;
  for (int i = numDigits-1; i >= 0; i--)
  {
    number += factor * digits[i];
    factor *= digitBases[i];
  }
  return number;
}

void
MultiBaseNumber::SetNumber(unsigned int number)
{
  unsigned int tmp = number;
  for (int i = numDigits-1; i >= 0; i--)
  {
    unsigned int mod = tmp % digitBases[i];
    digits[i] = mod;
    tmp -= mod;
    tmp /= digitBases[i];
  }
}

void
MultiBaseNumber::Inc(void)
{
  for (int i = numDigits-1; i >= 0; i--) // DON'T USE unsigned int, WE WANT WRAPAROUND LOGIC AND unsigned int BREAKS CODE
  {
    ++digits[i];
    if (digits[i] < digitBases[i])
      break;
    else
      digits[i] = 0;
  }
}

void
MultiBaseNumber::GetDigits(unsigned int* _digits) const
{
  memcpy((void*)_digits, (void*)digits, sizeof(unsigned int) * numDigits);
}
