/**
 * Copyright 2009 B. Schauerte. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

#pragma once

/**
 * @class MultiBaseNumber
 *
 * Represent numbers with digits, each digit can have an own "base".
 * This is especially useful if you have an array of arrays (each array has an own number of elements)
 * and want to iterate through all possible index combinations.
 *
 * @author Boris Schauerte
 * @date 2009
 */
class MultiBaseNumber
{
public:
  MultiBaseNumber(unsigned int _numDigits, unsigned int* _digitBases);
  ~MultiBaseNumber(void);

  /** Calculate digit representation for i+1. */
  void Inc(void);
  /** Calculate digit representation for i+1 using the prefix operator++ */
  inline MultiBaseNumber& operator++(void)
  {
    Inc();
    return *this;
  }
  /** Get the number represented by the current digits. */
  unsigned int GetNumber(void) const;
  /** Set digit representation of the number. */
  void SetNumber(unsigned int number);
  /** Get total number of numbers that can be represented by digits. */
  inline unsigned int GetTotal(void) const { return numTotal; }
  /** Get array of current digits. */
  inline const unsigned int* GetDigits(void) const { return digits; }
  /** Get a copy of the current digits. */
  void GetDigits(unsigned int* _digits) const;

  /** cout digits */
  void cout(void) const;
protected:
private:
  unsigned int* digitBases;    // the digit bases
  unsigned int* digits;        // the digits
  const unsigned int numDigits;// number of digits
  unsigned int numTotal;       // number of numbers that can be represented using digit combinations
};
