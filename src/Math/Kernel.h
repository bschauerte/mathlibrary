/**
 * Copyright 2008 J. Richarz. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are 
 * met:
 * 
 *    1. Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 * 
 *    2. Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in 
 *       the documentation and/or other materials provided with the 
 *       distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY B. SCHAUERTE ''AS IS'' AND ANY EXPRESS OR 
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL B. SCHAUERTE OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation
 * are those of the authors and should not be interpreted as representing 
 * official policies, either expressed or implied, of B. Schauerte.
 */

/*	File: kernel.h
	Kernel function representation
	Author: Jan Richarz
	Date: 2008/09/01
 */
#pragma once

#include "VectorND.h"
#include <cmath>
#include <cstdlib>

// kernel profile types
#define KERNEL_GAUSSIAN          (0)
#define KERNEL_LINEAR            (1)
#define KERNEL_EPANECHNIKOV      (2)
#define KERNEL_UNIFORM           (3)
#define KERNEL_UNDEFINED        (-1)

#define KERNEL_TYPE_ROTATIONAL   (0)
#define KERNEL_TYPE_MULTIVARIATE (1)

// Base class
class Kernel
{
public:
  Kernel(void);
  virtual ~Kernel(void);

  // Construct kernel
  // bandwidth: kernel bandwidths (sizes) for each dimension. Is equal to
  // the kernel width except gaussian kernel, where it is the variance.
  // dimensions: kernel dimensionality, dependent on the data
  // kernel: kernel function type
  void Init(float* bandwidth, int dimensions, int kernel = KERNEL_GAUSSIAN);
  void Init(float bandwidth, int dimensions, int kernel = KERNEL_GAUSSIAN);
  // get kernel function value based on difference vector
  // diff: difference vector between kernel center and data point
  // resolution: scaling factor allowing to use the same precomputed
  // kernel in different sizes. The kernel index will be divided
  // by resolution, i.e. a resolution value > 1.0 effectively results
  // in a larger kernel.
  // Version that expects a resolution value for each dimension
  virtual double GetWeighting(VectorND<double> diff, double* resolution) = 0;
  // convenience function
  virtual double GetWeighting(VectorND<double> center, VectorND<double> data, double* resolution) = 0;
  // Versions that take a single resolution value to be used for all dimensions
  virtual double GetWeighting(VectorND<double> diff, double resolution = 1.0) = 0;
  // convenience function
  virtual double GetWeighting(VectorND<double> center, VectorND<double> data, double resolution = 1.0) = 0;
  int Size(int idx = 0);
  virtual Kernel* GetDerivative(void) = 0;
  int GetType(void) {return m_kerneltype;}
  int Dim(void) {return m_dimensions;}
  float Bandwidth() {return m_bandwidth;}
  // Calculate Resolution value for wanted kernel size
  float GetResolution(float size) {return size/m_bandwidth;}

protected:
  // precompute kernel values
  void Precompute(void);
  // clear all variables
  void Clear(void);

  int m_kernelfunction;
  int m_dimensions;
  int m_kerneltype;
  float* m_kernelvalues;
  float* m_resolutions;
  float m_bandwidth;
  int m_kernelsize;
};

// A multivariate kernel, i.e. defined by independent 1D profiles that are multiplied
class KernelMultivariate
: public Kernel
{
public:
  KernelMultivariate(void);
  virtual ~KernelMultivariate(void);

  // Calculate weighting based on distance to center. Version with individual resolution for all dimensions...
  double GetWeighting(VectorND<double> diff, double* resolution);
  double GetWeighting(VectorND<double> center, VectorND<double> data, double* resolution);
  // ... and with single resolution applied to all dimensions
  double GetWeighting(VectorND<double> diff, double resolution = 1.0);
  double GetWeighting(VectorND<double> center, VectorND<double> data, double resolution = 1.0);
  Kernel* GetDerivative(void);

protected:
};

// Rotationally symmetric kernel
class KernelRotational
: public Kernel
{
public:
  KernelRotational(void);
  virtual ~KernelRotational(void);

  double GetWeighting(VectorND<double> diff, double* resolution);
  double GetWeighting(VectorND<double> center, VectorND<double> data, double* resolution);
  double GetWeighting(VectorND<double> diff, double resolution = 1.0);
  double GetWeighting(VectorND<double> center, VectorND<double> data, double resolution = 1.0);
  Kernel* GetDerivative(void);

protected:
};

